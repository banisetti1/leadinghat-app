import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';
import { RouterProvider } from 'react-router-dom';

import { Provider } from 'react-redux';
import router from './routes';
import store from './app/store';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

// const store = configureStore({
//   reducer: { [GET_PROFILE_FEATURE_KEY]: getProfileReducer },
//   // Additional middleware can be passed to this array
//   middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
//   devTools: process.env.NODE_ENV !== 'production',
//   // Optional Redux store enhancers
//   enhancers: [],
// });

root.render(
  <Provider store={store}>
    <StrictMode>
      <RouterProvider
        router={router}
        fallbackElement={
          <div>
            <h3>Loading...</h3>
          </div>
        }
      />
    </StrictMode>
  </Provider>
);
