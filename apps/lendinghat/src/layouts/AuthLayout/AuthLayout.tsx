import styles from './AuthLayout.module.scss';

/* eslint-disable-next-line */
export interface AuthLayoutProps {}

export function AuthLayout(props: AuthLayoutProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to AuthLayout!</h1>
    </div>
  );
}

export default AuthLayout;
