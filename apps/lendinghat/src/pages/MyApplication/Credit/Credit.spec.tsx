import { render } from '@testing-library/react';

import Credit from './Credit';

describe('Credit', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Credit />);
    expect(baseElement).toBeTruthy();
  });
});
