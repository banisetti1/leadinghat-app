import styles from './Credit.module.scss';

/* eslint-disable-next-line */
export interface CreditProps {}

export function Credit(props: CreditProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Credit!</h1>
    </div>
  );
}

export default Credit;
