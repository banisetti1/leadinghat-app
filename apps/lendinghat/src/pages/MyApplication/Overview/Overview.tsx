import styles from './Overview.module.scss';

/* eslint-disable-next-line */
export interface OverviewProps {}

export function Overview(props: OverviewProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Overview!</h1>
    </div>
  );
}

export default Overview;
