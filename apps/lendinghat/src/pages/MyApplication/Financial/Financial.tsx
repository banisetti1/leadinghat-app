import styles from './Financial.module.scss';

/* eslint-disable-next-line */
export interface FinancialProps {}

export function Financial(props: FinancialProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Financial!</h1>
    </div>
  );
}

export default Financial;
