import { render } from '@testing-library/react';

import Financial from './Financial';

describe('Financial', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Financial />);
    expect(baseElement).toBeTruthy();
  });
});
