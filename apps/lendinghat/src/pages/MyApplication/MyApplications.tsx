import { useState } from 'react';
import styles from './MyApplications.module.scss';
import { Outlet, useNavigate } from 'react-router-dom';

/* eslint-disable-next-line */
export interface MyApplicationProps {}

export function MyApplication(props: MyApplicationProps) {
  const navigate = useNavigate();
  const [activeTab, setActiveTab] = useState('OVERVIEW');

  const handleTabChange = (tabName: string, path: string) => {
    setActiveTab(tabName);
    navigate(`/dashboard/applications${path}`);
  };

  return (
    <div className={styles['container']}>
      <div className="flex justify-around space-x-2 items-center mt-20 border-b-2 border-blue-200">
        {activeTab === 'OVERVIEW' ? (
          <div className="bg-blue-200 w-full h-10 flex justify-center items-center border-b-4 border-blue-500 rounded-t-md">
            <h3 className="text-blue-500">Overview</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('OVERVIEW', '/')}
          >
            <h3 className="text-black">Overview</h3>
          </button>
        )}
        {activeTab === 'FINANCIAL' ? (
          <div className="bg-blue-200 w-full h-10 flex justify-center items-center border-b-4 border-blue-500 rounded-t-md">
            <h3 className="text-blue-500">Financial</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('FINANCIAL', '/financial')}
          >
            <h3 className="text-black">Financial</h3>
          </button>
        )}

        {activeTab === 'CREDIT' ? (
          <div className="bg-blue-200 w-full h-10 flex justify-center items-center border-b-4 border-blue-500 rounded-t-md">
            <h3 className="text-blue-500">Credit</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('CREDIT', '/credit')}
          >
            <h3 className="text-black">Credit</h3>
          </button>
        )}
        {activeTab === 'REPORT' ? (
          <div className="bg-blue-200 w-full h-10 flex justify-center items-center border-b-4 border-blue-500 rounded-t-md">
            <h3 className="text-blue-500">Report</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('REPORT', '/report')}
          >
            <h3 className="text-black">Report</h3>
          </button>
        )}
        {activeTab === 'PROFILE' ? (
          <div className="bg-blue-200 w-full h-10 flex justify-center items-center border-b-4 border-blue-500 rounded-t-md">
            <h3 className="text-blue-500">Profile</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('PROFILE', '/profile')}
          >
            <h3 className="text-black">Profile</h3>
          </button>
        )}
      </div>
      <Outlet></Outlet>
    </div>
  );
}

export default MyApplication;
