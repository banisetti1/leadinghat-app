import { render } from '@testing-library/react';

import MerchantRegistration from './MerchantRegistration';

describe('MerchantRegistration', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MerchantRegistration />);
    expect(baseElement).toBeTruthy();
  });
});
