import styles from './ForgotPassword.module.scss';

/* eslint-disable-next-line */
export interface ForgotPasswordProps {}

export function ForgotPassword(props: ForgotPasswordProps) {
  return (
    <div className={styles['container']}>
      <h1 className={styles.lh_heading}>Forgot Password</h1>
      <p className={styles.lh_subheading}>
        Verification code send on register email address
      </p>
      <div className={styles.lh_blog_post_data}>
        <form>
          <div className="mb-6">
            <input
              type="email"
              id="email"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-12 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Work Email Address"
              required
            />
          </div>
          <button
            type="submit"
            className={`${styles.lh_u_btn} ${styles.lh_u_btn__solid} ${styles.lh_u_btn__solid__icon} ${styles.lh_u_btn__rounded} w-full`}
          >
            Send verification code
          </button>
        </form>
      </div>
    </div>
  );
}

export default ForgotPassword;
