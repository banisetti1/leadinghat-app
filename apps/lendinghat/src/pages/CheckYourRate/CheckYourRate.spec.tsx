import { render } from '@testing-library/react';

import CheckYourRate from './CheckYourRate';

describe('CheckYourRate', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CheckYourRate />);
    expect(baseElement).toBeTruthy();
  });
});
