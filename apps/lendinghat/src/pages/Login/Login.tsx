import { Header } from '@lendinghat/common-ui';
import styles from './Login.module.scss';

import { useDispatch } from 'react-redux';
import {
  fetchGetProfile,
  getProfileActions,
} from '../../app/store/get-profile.slice';
import { authActions, loginUser } from '../../app/store/slices/auth.slice';
import { useAppDispatch } from '../../app/store/hooks';
import { useNavigate } from 'react-router-dom';

import logoUrl from '../../assets/logo.png';

/* eslint-disable-next-line */
export interface LoginProps {}

export function Login(props: LoginProps) {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();

  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    dispatch(
      loginUser({
        email: 'eve.holt@reqres.in',
        password: 'cityslicka',
      })
    ).then((res) => {
      if (res?.payload?.token) {
        navigate('/dashboard', { replace: true });
      }
    });
  };
  return (
    <div className="flex flex-row">
      <div className="w-3/5 h-screen bg-blue-200">
        <h3>images</h3>
      </div>
      <div className="w-2/5 h-screen flex justify-center items-center">
        <div className="flex-col justify-between items-center">
          <div className="flex flex-col h-1/3 justify-between items-center">
            <img
              className="mb-10"
              src={logoUrl}
              alt="lending hat logo"
              height={55.45}
              width={202}
            />
            <h1 className={styles.lh_heading}>Welcome Back</h1>
            <p className={styles.lh_subheading}>Log into your account</p>
          </div>

          <div className={styles.lh_blog_post_data}>
            <form>
              <div className="mb-6">
                <input
                  type="email"
                  id="email"
                  className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-12 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Email"
                  required
                />
              </div>
              <div className="mb-6">
                <input
                  type="password"
                  id="password"
                  className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-12 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  placeholder="Password"
                  required
                />
              </div>
              <div className="flex items-start mb-6">
                <div className="flex items-center h-5">
                  <input
                    id="remember"
                    type="checkbox"
                    value=""
                    className="w-4 h-4 border border-lh-border-color rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
                    required
                  />
                </div>
                <label
                  htmlFor="remember"
                  className="flex flex-1 w-fit ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
                >
                  Remember me
                </label>
                <a href="/forgot" className="text-blue-500 text-sm font-normal">
                  Forgot Password?
                </a>
              </div>

              <button
                type="submit"
                onClick={handleSubmit}
                className={`${styles.lh_u_btn} ${styles.lh_u_btn__solid} ${styles.lh_u_btn__solid__icon} ${styles.lh_u_btn__rounded} w-full`}
              >
                Login
              </button>
            </form>
            <div className="flex justify-center items-center flex-col">
              <div className="justify-center my-3">
                <span className="">OR</span>
              </div>
              <button
                type="submit"
                onClick={handleSubmit}
                className={`${styles.lh_u_btn} outline outline-2 outline-blue-600 ${styles.lh_u_btn__solid__icon} ${styles.lh_u_btn__rounded} w-full`}
              >
                <span className="text-blue-600">Create Application</span>
              </button>
              <div className="flex flex-col justify-center items-center mt-8">
                <h3 className="text-sm mb-1">Don't have an account?</h3>
                <h3 className="text-sm text-blue-600 font-semibold underline mt-1">
                  Signup and check your Rate
                </h3>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;
