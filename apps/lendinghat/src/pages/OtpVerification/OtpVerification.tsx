import styles from './OtpVerification.module.scss';

/* eslint-disable-next-line */
export interface OtpVerificationProps {}

export function OtpVerification(props: OtpVerificationProps) {
  return (
    <div className={styles['container']}>
      <h1 className={styles.lh_heading}>Verification</h1>
      <p className={styles.lh_subheading}>
        We have just sent an 4 digit verification code to devid......@gmail.com
      </p>
      <div className={styles.lh_blog_post_data}>
        <form>
          <div className="flex flex-row justify-evenly mb-6">
            <input
              type="email"
              id="email"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-24 w-24 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
            />
            <input
              type="email"
              id="email"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-24 w-24 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
            />
            <input
              type="email"
              id="email"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-24 w-24 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
            />
            <input
              type="email"
              id="email"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-24 w-24 p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              required
            />
          </div>

          <div className="flex flex-col justify-center items-center mb-2 mt-8">
            <p>
              Time left <span className="text-red-500">03:59</span>
            </p>
            <p>
              Don't receive the code?{' '}
              <span className="text-blue-500">Resend</span>
            </p>
          </div>

          <button
            type="submit"
            className={`${styles.lh_u_btn} ${styles.lh_u_btn__solid} ${styles.lh_u_btn__solid__icon} ${styles.lh_u_btn__rounded} w-full`}
          >
            Verify code
          </button>
        </form>
      </div>
    </div>
  );
}

export default OtpVerification;
