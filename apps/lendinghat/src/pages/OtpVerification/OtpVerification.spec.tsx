import { render } from '@testing-library/react';

import OtpVerification from './OtpVerification';

describe('OtpVerification', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<OtpVerification />);
    expect(baseElement).toBeTruthy();
  });
});
