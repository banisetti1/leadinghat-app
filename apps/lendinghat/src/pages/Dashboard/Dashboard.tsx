import { Navigate, Outlet } from 'react-router-dom';
import styles from './Dashboard.module.scss';

import LogoUrl from '../../assets/logo_light.png';
import ProfileUrl from '../../assets/profile.png';
import SearchIcon from '../../assets/search.svg';
import Search from '../../app/components/Search/Search';
import { useAppSelector } from '../../app/store/hooks';
import { useState } from 'react';
import { Button } from '@lendinghat/common-ui';

/* eslint-disable-next-line */
export interface DashboardProps {}

export function Dashboard(props: DashboardProps) {
  const token = useAppSelector((state) => state.auth.token);

  if (!token) {
    return <Navigate to="/" />;
  }
  return (
    <div className={styles['container']}>
      <h3>Dashboard</h3>

      {/* // <Search
    //   onSearch={function (event: string): void {
    //     throw new Error('Function not implemented.');
    //   }}
    //   label={'Search Something'}
    //   searchIcon={SearchIcon}
    //   searchTerm={''}
    // >
    //   <h3>Dashboard</h3>
    // </Search> */}
    </div>
  );
}

export default Dashboard;
