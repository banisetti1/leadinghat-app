import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  PayloadAction,
} from '@reduxjs/toolkit';

export const AUTH_FEATURE_KEY = 'auth';

/*
 * Update these interfaces according to your requirements.
 */
export interface AuthEntity {
  token: string;
}

export interface AuthState {
  loadingStatus: 'not loaded' | 'loading' | 'loaded' | 'error';
  error?: string | null;
  token: string | null;
  isAuthenticated: boolean;
}

const initialState: AuthState = {
  loadingStatus: 'not loaded',
  error: null,
  token: null,
  isAuthenticated: false,
};

export const authAdapter = createEntityAdapter();

/**
 * Export an effect using createAsyncThunk from
 * the Redux Toolkit: https://redux-toolkit.js.org/api/createAsyncThunk
 *
 * e.g.
 * ```
 * import React, { useEffect } from 'react';
 * import { useDispatch } from 'react-redux';
 *
 * // ...
 *
 * const dispatch = useDispatch();
 * useEffect(() => {
 *   dispatch(fetchAuth())
 * }, [dispatch]);
 * ```
 */
export const loginUser = createAsyncThunk<any>(
  'auth/fetchStatus',
  async (args, thunkAPI) => {
    try {
      console.log('thunkAPI', args, thunkAPI);
      const response = await fetch(`https://reqres.in/api/login`, {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(args),
      });

      if (!response.ok) {
        throw new Error(`Could not fetch, received ${response.status}`);
      }

      return await response.json();
    } catch (err) {
      return thunkAPI.rejectWithValue({
        message: err,
      });
    }
  }
);

// export const initialAuthState: AuthState = authAdapter.getInitialState({
//   loadingStatus: 'not loaded',
//   error: null,
// });

export const authSlice = createSlice({
  name: AUTH_FEATURE_KEY,
  initialState,
  reducers: {
    // addToken: (state, action: PayloadAction<{token: string}>) => {
    //   state.token = action.payload.token;
    // },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loginUser.pending, (state: AuthState) => {
        state.loadingStatus = 'loading';
      })
      .addCase(
        loginUser.fulfilled,
        (state: AuthState, action: PayloadAction<any>) => {
          state.token = action.payload.token;
          state.loadingStatus = 'loaded';
        }
      )
      .addCase(loginUser.rejected, (state: AuthState, action) => {
        state.loadingStatus = 'error';
        state.error = action.error.message;
        state.token = null;
      });
  },
});

/*
 * Export reducer for store configuration.
 */
export const authReducer = authSlice.reducer;

/*
 * Export action creators to be dispatched. For use with the `useDispatch` hook.
 *
 * e.g.
 * ```
 * import React, { useEffect } from 'react';
 * import { useDispatch } from 'react-redux';
 *
 * // ...
 *
 * const dispatch = useDispatch();
 * useEffect(() => {
 *   dispatch(authActions.add({ id: 1 }))
 * }, [dispatch]);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#usedispatch
 */
export const authActions = authSlice.actions;

/*
 * Export selectors to query state. For use with the `useSelector` hook.
 *
 * e.g.
 * ```
 * import { useSelector } from 'react-redux';
 *
 * // ...
 *
 * const entities = useSelector(selectAllAuth);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#useselector
 */
const { selectAll, selectEntities } = authAdapter.getSelectors();

export const getAuthState = (rootState: {
  [AUTH_FEATURE_KEY]: AuthState;
}): AuthState => rootState[AUTH_FEATURE_KEY];

// export const selectAllAuth = createSelector(getAuthState, selectAll);

// export const selectAuthEntities = createSelector(getAuthState, selectEntities);
