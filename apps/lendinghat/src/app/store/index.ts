import { configureStore } from '@reduxjs/toolkit';
import {
  GET_PROFILE_FEATURE_KEY,
  getProfileReducer,
} from './get-profile.slice';
import { createLogger } from 'redux-logger';
import { AUTH_FEATURE_KEY, authReducer } from './slices/auth.slice';

const logger = createLogger({
  duration: true,
  collapsed: false,
  diff: true,
  colors: {
    title: () => '#7F00FF',
    prevState: () => '#596869',
    action: () => '#1481BA',
    nextState: () => '#149945',
    error: () => '#ff0005',
  },
});

export const store = configureStore({
  reducer: {
    [GET_PROFILE_FEATURE_KEY]: getProfileReducer,
    [AUTH_FEATURE_KEY]: authReducer,
  },
  // Additional middleware can be passed to this array
  middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger),
  devTools: process.env.NODE_ENV !== 'production',
  // Optional Redux store enhancers
  enhancers: [],
});

export default store;

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
