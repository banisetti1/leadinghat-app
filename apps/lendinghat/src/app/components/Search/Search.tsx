import { useCallback, useState } from 'react';
import styles from './Search.module.scss';

/* eslint-disable-next-line */
export interface SearchProps {
  children: React.ReactNode;
  onSearch: (event: string) => void;
  label: string;
  searchIcon: string;
  searchTerm: string;
}

export function Search(props: SearchProps) {
  const { children, label, onSearch, searchIcon, searchTerm } = props;
  const [search, setSearch] = useState('');

  const handleOnSearch = useCallback(() => {
    // debounced(e);
    onSearch(search);
  }, [onSearch, search]);
  return (
    <div className={styles.lh_blog_post_section__inner_area__form__right}>
      <div
        className={`${styles.lh_blog_search_area} ${styles.lh_search_wrapper} ${styles.lh_search_wrapper} box-shadow: 0px 4px 25px rgba(20, 107, 255, 0.1)`}
      >
        <input
          type="text"
          id="mySearch"
          // onChange={(event) => handleOnSearch(event)}
          onChange={(e) => setSearch(e.target.value)}
          onKeyDown={(e: React.KeyboardEvent) => {
            if (e.key === 'Enter') {
              e.preventDefault();
              handleOnSearch();
            }
          }}
          placeholder={label}
          title="Type in a category"
          defaultValue={searchTerm ?? ''}
        />
        <button
          type="button"
          className={styles.lh_blog_search_btn}
          onClick={() => handleOnSearch()}
        >
          <img
            src={searchIcon}
            className={styles.lh_blog_search_icon}
            alt="search icon"
          />
        </button>
      </div>
      {children}
    </div>
  );
}

export default Search;
