import { render } from '@testing-library/react';

import Search from './Search';

describe('Search', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <Search
        children={undefined}
        onSearch={function (event: string): void {
          throw new Error('Function not implemented.');
        }}
        label={''}
        searchIcon={''}
        searchTerm={''}
      />
    );
    expect(baseElement).toBeTruthy();
  });
});
