import { createBrowserRouter } from 'react-router-dom';
import App from './app/app';
import Login from './pages/Login/Login';
import Registration from './pages/Registration/Registration';
import ForgotPassword from './pages/ForgotPassword/ForgotPassword';
import OtpVerification from './pages/OtpVerification/OtpVerification';
import Dashboard from './pages/Dashboard/Dashboard';
import DashboardLayout from './layouts/DashboardLayout/DashboardLayout';
import AnalyticsPage from './pages/Analytics/AnalyticsPage';
import MyApplication from './pages/MyApplication/MyApplications';
import Overview from './pages/MyApplication/Overview/Overview';
import Financial from './pages/MyApplication/Financial/Financial';
import Credit from './pages/MyApplication/Credit/Credit';
import Profile from './pages/MyApplication/Profile/Profile';
import Report from './pages/MyApplication/Report/Report';
import CheckYourRate from './pages/CheckYourRate/CheckYourRate';
import MerchantHome from './pages/MerchantHome/MerchantHome';
import MerchantRegistration from './pages/MerchantRegistration/MerchantRegistration';
import Application from './pages/Application/Application';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    children: [
      {
        index: true,
        // path: '/login',
        element: <Login />,
      },
      {
        path: '/register',
        element: <Registration />,
      },
      {
        path: '/forgot',
        element: <ForgotPassword />,
      },
      {
        path: '/verify',
        element: <OtpVerification />,
      },
      {
        path: '/check-your-rate',
        element: <CheckYourRate />,
      },
    ],
  },

  {
    path: '/MerchantHome',
    element: <MerchantHome />,
  },
  {
    path: '/MerchantRegistration',
    element: <MerchantRegistration />,
  },

  {
    path: '/dashboard',
    element: <DashboardLayout />,
    children: [
      {
        index: true,
        element: <Dashboard />,
      },
      {
        path: 'analytics',
        element: <AnalyticsPage />,
      },
      {
        path: 'Application',
        element: <Application />,
      },
      {
        path: 'applications',
        element: <MyApplication />,
        children: [
          {
            index: true,
            element: <Overview />,
          },
          {
            path: 'financial',
            element: <Financial />,
          },
          {
            path: 'credit',
            element: <Credit />,
          },
          {
            path: 'report',
            element: <Report />,
          },
          {
            path: 'profile',
            element: <Profile />,
          },
        ],
      },
    ],
  },
  // {
  //   path: 'register',
  //   element: <Registration />,
  // },
  // {
  //   path: 'forgot',
  //   element: <ForgotPassword />,
  // },
  // {
  //   path: 'verify',
  //   element: <OtpVerification />,
  // },
]);

export default router;
