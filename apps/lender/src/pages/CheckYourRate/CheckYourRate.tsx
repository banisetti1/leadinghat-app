import { CheckBox, Container, Text } from '@lendinghat/common-ui';
import styles from './CheckYourRate.module.scss';

/* eslint-disable-next-line */
export interface CheckYourRateProps {}

export function CheckYourRate(props: CheckYourRateProps) {
  return (
    <Container style={styles.test_style as any}>
      <h1 className={styles.lh_heading}>Check Your Rate</h1>

      <Text as={'h1'}>Check Your Rate as Text</Text>
    </Container>
  );
}

export default CheckYourRate;
