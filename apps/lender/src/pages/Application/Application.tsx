import styles from './Application.module.scss';

import {Table, Text, Button} from '@lendinghat/common-ui';

import TotalApplicationGraph from '../../assets/total-application-graph.svg';
import appOverviewIcon from '../../assets/application-overview-icon.svg';

/* eslint-disable-next-line */
export interface ApplicationProps {}

export function Application(props: ApplicationProps) {
  return (
    <div>
       <div className={styles.lender_dashboard_route_title}>
         <Text as={`h5`}>Applications</Text>
       </div>

       <div className={styles.lender_dashboard_application_status}>

        <div className={styles.lh_card_light_shadow}>
          <div className={styles.lh_card_data}>
            <p>Total Applications</p>
            <Text as={`h5`}>302</Text>
          </div>
          <div className={styles.lh_card_data}>
            <img src={TotalApplicationGraph} />
          </div>
        </div>

        <div className={styles.lh_card_light_shadow}>
          <div className={styles.lh_card_data}>
            <p>Need Review</p>
            <Text as={`h5`}>105</Text>
          </div>
        </div>

        <div className={styles.lh_card_light_shadow}>
          <div className={styles.lh_card_data}>
            <p>Approved</p>
            <Text as={`h5`}>97</Text>
          </div>
        </div>

        <div className={styles.lh_card_light_shadow}>
          <div className={styles.lh_card_data}>
            <p>Declined</p>
            <Text as={`h5`}>100</Text>
          </div>
        </div>

       </div>

       <div className={styles.lender_dashboard_application_filter}>
          <div className={styles.lender_dashboard_application_filter_wrapper}>
            <div className={styles.lender_dashboard_application_filter_label}>
                <label htmlFor="loan_source">Filter by: </label>
                <select id="loan_source" className={styles.select_dropdown}>
                  <option>Loan Source</option>
                  <option>All</option>
                  <option>Lending Hat</option>
                  <option>Merchant</option>
                  <option>Broker</option>
                </select>
            </div>
            <select className={styles.select_dropdown_industry}>
              <option>Industry</option>
              <option>Medical practices</option>
              <option>Farm equipment</option>
              <option>Construction</option>
              <option>Commercial vehicles</option>
              <option>Medical equipment</option>
            </select>
            <select className={styles.select_dropdown}>
              <option>Offer Status</option>
              <option>All</option>
              <option>Need Review</option>
              <option>Approved</option>
              <option>Declined</option>
            </select>
          </div>

            <Button
              label="Underwrite All"
              variant="primary"
              className={styles.lender_dashboard_application_btn}
            />
       </div>

       <div className={styles.lender_dashboard_application_table_wrapper}>
        <Table></Table>
       </div>
    </div>
  );
}

export default Application;
