import { render } from '@testing-library/react';

import Application from './Application';

describe('Application', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Application />);
    expect(baseElement).toBeTruthy();
  });
});
