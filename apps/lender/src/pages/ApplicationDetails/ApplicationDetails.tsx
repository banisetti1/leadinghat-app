import styles from './ApplicationDetails.module.scss';

/* eslint-disable-next-line */
export interface ApplicationDetailsProps {}

export function ApplicationDetails(props: ApplicationDetailsProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to ApplicationDetails!</h1>
    </div>
  );
}

export default ApplicationDetails;
