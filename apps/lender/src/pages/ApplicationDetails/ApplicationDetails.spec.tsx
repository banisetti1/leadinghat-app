import { render } from '@testing-library/react';

import ApplicationDetails from './ApplicationDetails';

describe('ApplicationDetails', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<ApplicationDetails />);
    expect(baseElement).toBeTruthy();
  });
});
