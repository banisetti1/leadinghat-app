import styles from './OtpVerification.module.scss';

import Header from '../../layouts/Headers/Headers/Headers';
import { Button, Card } from '@lendinghat/common-ui';

import BtnNxtArrow from '../../assets/nxt-arrow.svg';

/* eslint-disable-next-line */
export interface OtpVerificationProps {}

export function OtpVerification(props: OtpVerificationProps) {
  return (
    <div>
      <Header />

      <main>
        <div className={styles.lender_login_section}>
          <h1 className={styles.lender_login_section__heading}>
            Forgot Password
          </h1>
          <p className={styles.lender_login_section__para}>
            We have just sent an 4 digit verification code to
            devid........@gmail.com
          </p>

          <div className={styles.lender_login_panel}>
            <form className={styles.lender_login__form}>
              <div className={styles.lender_login__form_group}>
                <input
                  type="number"
                  className="lender_num_otp input_field_type_II"
                  id=""
                />
                <input
                  type="number"
                  className="lender_num_otp input_field_type_II"
                  id=""
                />
                <input
                  type="number"
                  className="lender_num_otp input_field_type_II"
                  id=""
                />
                <input
                  type="number"
                  className="lender_num_otp input_field_type_II"
                  id=""
                />
              </div>

              <p className={styles.lender_login_otptime_para}>
                Time left:
                <span className={styles.lender_login_otptime_count}>03:59</span>
              </p>
              <p className={styles.lender_login_otptime_resend_para}>
                Don’t receive the code?
                <a href="#" className={styles.lender_login_otptime_resend_code}>
                  Resend
                </a>
              </p>

              <Button
                label="Verify code"
                onClick={() => {}}
                variant="primary"
                className={styles.lender_login_panel_btn}
              />
            </form>
          </div>
        </div>
      </main>

      <div className={styles.lh_copyright}>
        <p className={styles.lh_copyright_para}>
          Copyright 2022-23 Lending Hat | All Rights Reserved
        </p>
      </div>
    </div>
  );
}

export default OtpVerification;
