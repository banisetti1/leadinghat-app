import styles from './ForgotPassword.module.scss';

import Header from '../../layouts/Headers/Headers/Headers';
import { Button, Card } from '@lendinghat/common-ui';

import BtnNxtArrow from '../../assets/nxt-arrow.svg';

/* eslint-disable-next-line */
export interface ForgotPasswordProps {}

export function ForgotPassword(props: ForgotPasswordProps) {
  return (
    <div>
      <Header />

      <main>
        <div className={styles.lender_login_section}>
          <h1 className={styles.lender_login_section__heading}>
            Forgot Password
          </h1>
          <p className={styles.lender_login_section__para}>
            Verification code send on register email address
          </p>

          <div className={styles.lender_login_panel}>
            <form className={styles.lender_login__form}>
              <div className={styles.lender_login_form_group}>
                <input
                  type="email"
                  className="lender_login_mail input_field_type_I"
                  id=""
                  placeholder="Work Email Address*"
                />
              </div>

              <Button
                label="Send verification code"
                onClick={() => {}}
                variant="primary"
                className={styles.lender_login_panel_btn}
              />
            </form>
          </div>
        </div>
      </main>

      <div className={styles.lh_copyright}>
        <p className={styles.lh_copyright_para}>
          Copyright 2022-23 Lending Hat | All Rights Reserved
        </p>
      </div>
    </div>
  );
}

export default ForgotPassword;
