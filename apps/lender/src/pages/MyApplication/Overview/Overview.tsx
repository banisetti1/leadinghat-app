import styles from './Overview.module.scss';

import ApplicationIcon from '../../../assets/application-icon.svg';
import CheckedSuccess from '../../../assets/checked-success.svg';
import revenuGraph from '../../../assets/app-overview-revenue-bg.svg';
import RealTimeBalanceIcon from '../../../assets/real-time-balance-icon.svg';
import CredScoreIcon from '../../../assets/credit-score-icon.svg';
import DateDarkIcon from '../../../assets/date-icon-dark.svg';
import RealTimeBg from '../../../assets/application-real-time-balance-bg.svg';
import ScoreMeter from '../../../assets/credit-score-meter-icon.svg';
import exportBtnIcon from '../../../assets/document-export-icon.svg';
import LoanSummaryIcon from '../../../assets/loan-summary-icon.svg';
import LoanSummaryGraph from '../../../assets/loan-summary-graph.svg';
import RevenueIcon from '../../../assets/revenue-icon.svg';


 
import {Text, Button} from '@lendinghat/common-ui';

/* eslint-disable-next-line */
export interface OverviewProps {}

export function Overview(props: OverviewProps) {
  return (
    <div>
      
       <div className={styles.lender_dashboard_feature}>

       <div className={styles.lender_dashboard_feature_40}>
          <div className={styles.lh_card_light_b_shadow}>
            <div className={styles.lender_dashboard_feature_40__title}>
                <div className={styles.feature_title_icon}>
                  <img src={ApplicationIcon}/>
                </div>
                <Text as={`p_black`}>Application For</Text>
            </div>

            <div className={styles.lender_dashboard_feature_40_data}>
              <p>Applicant name</p>
              <div className={styles.lender_dashboard_feature_40_data_content}>
                <Text as={`h5`}>Michael Manley</Text>
                <img src={CheckedSuccess}/>
              </div>
            </div>

            <div className={styles.lender_dashboard_feature_40_data}>
              <p>Requested Loan Amount</p>
              <div className={styles.lender_dashboard_feature_40_data_content}>
                <Text as={`h5`}>$50,000</Text>
              </div>
            </div>

            <div className={styles.lender_dashboard_feature_40_data}>
              <p>Industry</p>
              <div className={styles.lender_dashboard_feature_40_data_content}>
                <Text as={`h5`}>Construction</Text>
              </div>
            </div>

          </div>
       </div>

        <div className={styles.lender_dashboard_feature_60}>
          <div className={styles.lh_card_light_b_shadow}>
            <div className={styles.lender_dashboard_feature_60__title}>
              <div className={styles.revenue_title}>
                <div className={styles.revenue_icon}>
                  <div className={styles.revenue_icon_box}><img src={RevenueIcon}/></div>
                </div>
                <div className={styles.revenue_text}>
                  <Text as={`p_black`}>Average Revenue</Text>
                  <Text as={`h5`}>$65000</Text>
                </div>
              </div>

              <input
                type="date"
                className={styles.input_field_type_b_dark}
                placeholder="last 6 months"
              />
            </div>

            <img src={revenuGraph}/>
          </div>
        </div>
        
      </div>

      <div className={styles.lender_dashboard_feature}>
       <div className={styles.lender_dashboard_feature_70}>
          <div className={styles.lh_card_light_b_shadow}>
            <div className={styles.lender_dashboard_feature_70__title}>
                <div className={styles.feature_title_icon}>
                  <img src={RealTimeBalanceIcon}/>
                </div>
                <Text as={`p_black`}>Real-time balance data & Weekday Average Balance </Text>
            </div>

            <div className={styles.realtime_date_area}>
              <div className={styles.realtime_date}>
                <label htmlFor={`from_date`}>Start date</label>
                <input type='date' id='from_date'/>
              </div>

              <div className={styles.realtime_date}>
                <p className={styles.p}>to</p>
              </div>

              <div className={styles.realtime_date}>
                <label htmlFor={`from_date`}>Start date</label>
                <input type='date' id='from_date'/>
              </div>
            </div>
            <img src={RealTimeBg} />
          </div>
       </div>

        <div className={styles.lender_dashboard_feature_30}>
          <div className={styles.lh_card_light_b_shadow}>
            <div className={styles.lender_dashboard_feature_30__title}>
                <div className={styles.feature_title_icon}>
                  <img src={CredScoreIcon}/>
              </div>
              <Text as={`p_black`}>Credit Status</Text>
            </div>

            <img src={ScoreMeter}/>

            <div className={styles.score_data_info}>
              <div className={styles.score_data}>
                <p>Business Credit Score</p>
                <Text as={`h5`}>689</Text>
              </div>

              <div className={styles.score_data}>
                <p>Personal Credit Score</p>
                <Text as={`h5`}>650</Text>
              </div>

              <div className={styles.score_data}>
                <p>Business Intelliscore</p>
                <Text as={`h5`}>640</Text>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className={styles.lender_dashboard_application_deposite_card}>
        <div className={styles.lh_card_light_b_shadow}>
           <div className={styles.lender_dashboard_application_deposite_card_data}>
            <p>No. of NSF days</p>
            <Text as={`h5`}>5</Text>
           </div>
        </div>
        
        <div className={styles.lh_card_light_b_shadow}>
          <div className={styles.lender_dashboard_application_deposite_card_data}>
            <p>No. of Negative days</p>
            <Text as={`h5`}>2</Text>
          </div>
        </div>
        
        <div className={styles.lh_card_light_b_shadow}>
          <div className={styles.lender_dashboard_application_deposite_card_data}>
            <p>Total Deposit</p>
            <Text as={`h5`}>560</Text>
          </div>
          <div className={styles.lender_dashboard_application_deposite_card_data}>
            <input type='date' className={styles.input_field_type_b_dark}/>
          </div>
        </div>

      </div>

      <div className={styles.lender_dashboard_application_loan_summary}>
        <div className={styles.lender_dashboard_application_loan_summary_wrapper}>
          <div className={styles.lender_dashboard_application_loan_summary_title_area}>
            <div className={styles.lender_dashboard_application_loan_summary_title}>
              <div className={styles.title_icon}><img src={LoanSummaryIcon}/></div>
              <Text as={`p_black`}>Loan Summary</Text>
            </div>

            <div className={styles.lender_dashboard_application_loan_summary_fields}>
            <div className={styles.dropdown_selected_data}>
              <p>Current Loan: <span>2</span></p>
            </div>
            <select className={styles.select_loan}>
              <option>Choose Loan</option>
              <option>Choose Loan 1</option>
              <option>Choose Loan 2</option>
              <option>Choose Loan 3</option>
              <option>Choose Loan 4</option>
            </select>
            </div>
          </div>      

          <div className={styles.lender_dashboard_application_loan_summary_data}>
            <div className={styles.lender_dashboard_application_loan_summary_total}>
              <ul className={styles.total_list}>
                <li>
                  <p className={styles.p_subtitle}>Loan Amount <Button label="Active" variant="outline-success" className={styles.active_btn}/></p>
                  <Text as={`h5`}>$30,000</Text>
                </li>

                <li>
                  <p className={styles.p_subtitle}>Yearly rate of interest</p>
                  <p className={styles.p_title}>25%</p>
                </li>
              </ul>

              <ul className={styles.loan_repayment}>
                <li><p>Loan repayment Probability</p></li>
                <li><Text as={`h5`}>0.95</Text> <input type='range'/></li>
              </ul>
            </div>

            <div className={styles.lender_dashboard_application_loan_summary_graph}>
              <div className={styles.lender_dashboard_application_loan_summary_graph_content}>
                <ul className={styles.total_list}>
                  <li>
                    <p className={styles.p_subtitle}>Repayment amount</p>
                    <Text as={`h5`}>$781.25</Text>
                  </li>
                  <li>
                    <p className={styles.p_subtitle}>Repayment Type</p>
                    <Text as={`h5`}>Weekly</Text>
                  </li>
                  <li>
                    <p className={styles.p_subtitle}>Yearly rate of interest</p>
                    <p className={styles.p_title}>$7458</p>
                  </li>
                </ul>
                <img src={LoanSummaryGraph}/>
              </div>
            </div>

            <div className={styles.lender_dashboard_application_loan_summary_type_info}>
            <ul className={styles.total_list}>
                  <li>
                    <p className={styles.p_subtitle}>Loan Type</p>
                    <p className={styles.p_title}>Term Loan</p>
                  </li>
                  <li>
                    <p className={styles.p_subtitle}>Term</p>
                    <p className={styles.p_title}>8 months</p>
                  </li>
                  <li>
                    <p className={styles.p_subtitle}>Loan start date</p>
                    <p className={styles.p_title}>05-26-2023</p>
                  </li>
                  <li>
                    <p className={styles.p_subtitle}>Remaining Time</p>
                    <p className={styles.p_title}>3 Months</p>
                  </li>
                </ul>
            </div>
          </div>
        </div>
          
      </div>

    </div>
  );
}

export default Overview;
