import { useState } from 'react';
import styles from './MyApplications.module.scss';
import { Outlet, useNavigate } from 'react-router-dom';

import {Table, Text, Button} from '@lendinghat/common-ui';

import TotalApplicationGraph from '../../assets/total-application-graph.svg';
import appOverviewIcon from '../../assets/application-overview-icon.svg';
import exportBtnIcon from '../../assets/document-export-icon.svg';


/* eslint-disable-next-line */
export interface MyApplicationProps {}

export function MyApplication(props: MyApplicationProps) {
  const navigate = useNavigate();
  const [activeTab, setActiveTab] = useState('OVERVIEW');

  const handleTabChange = (tabName: string, path: string) => {
    setActiveTab(tabName);
    navigate(`/dashboard/applications${path}`);
  };

  return (
    <div>
      <div className={styles.lender_dashboard_route_title}>
        <Text as={`h5`}>Application Details</Text>

        <ul className={styles.lender_dashboard_route_list}>
           <li><p>APPLICATION ID: # BLC39856</p></li>
           <li>
              <p><Button label="Need Review" variant="link" className={styles.data_review_btn}/>
              </p>
           </li>
           <li><a href='#' className={styles.data_export_btn}><img src={exportBtnIcon} /></a></li>
        </ul>
      </div>

      <div className={styles.lender_dashboard_route_construction}>
        <p className={styles.lender_dashboard_route_construction_title}>Manley Construction</p>
        <div className={styles.lender_dashboard_route_construction_btn_area}>
        <Button label="Underwrite" variant="primary" className={styles.lender_dashboard_route_construction_btn_primery}/>
        <Button label="Make An Offer" variant="outline-dark" className={styles.lender_dashboard_route_construction_btn_dark_outline}/>
        </div>
      </div>
       
       <div className={styles.lender_dashboard_application_details_tab}>
        {activeTab === 'OVERVIEW' ? (
          <div className={styles.lender_dashboard_application_details_tab_btn_active}>
            <h3 className={styles.lender_dashboard_application_details_tab_btn_active_txt}>
            <svg width="15" height="20" viewBox="0 0 15 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.7157 19.2149H2.2843C1.67869 19.2142 1.09809 18.9733 0.669856 18.545C0.241623 18.1168 0.000723841 17.5362 0 16.9306V3.17371C0.000723841 2.56809 0.241623 1.98749 0.669856 1.55926C1.09809 1.13103 1.67869 0.890128 2.2843 0.889404H9.12673C9.21654 0.889364 9.30548 0.907016 9.38847 0.941353C9.47145 0.975691 9.54687 1.02604 9.61039 1.08953L14.7999 6.28083C14.8634 6.34436 14.9137 6.41977 14.948 6.50276C14.9824 6.58575 15 6.67469 15 6.7645V16.9301C14.9994 17.5358 14.7586 18.1165 14.3303 18.5449C13.9021 18.9732 13.3214 19.2142 12.7157 19.2149ZM2.2843 2.25698C2.04125 2.25722 1.80821 2.35388 1.63634 2.52575C1.46448 2.69762 1.36781 2.93065 1.36757 3.17371V16.9306C1.36781 17.1736 1.46448 17.4067 1.63634 17.5785C1.80821 17.7504 2.04125 17.8471 2.2843 17.8473H12.7157C12.9588 17.8471 13.1918 17.7504 13.3637 17.5785C13.5355 17.4067 13.6322 17.1736 13.6324 16.9306V7.04758L8.84182 2.25698H2.2843Z" fill=""/>
                <path d="M14.3165 7.44828H10.7257C10.1201 7.44756 9.5395 7.20666 9.11126 6.77843C8.68303 6.35019 8.44213 5.76959 8.44141 5.16398V1.57319C8.44141 1.39184 8.51345 1.21792 8.64168 1.08968C8.76992 0.961446 8.94384 0.889404 9.12519 0.889404C9.30654 0.889404 9.48047 0.961446 9.6087 1.08968C9.73694 1.21792 9.80898 1.39184 9.80898 1.57319V5.16398C9.80922 5.40704 9.90588 5.64007 10.0777 5.81194C10.2496 5.98381 10.4827 6.08047 10.7257 6.08071H14.3165C14.4978 6.08071 14.6718 6.15275 14.8 6.28099C14.9282 6.40922 15.0003 6.58314 15.0003 6.7645C15.0003 6.94585 14.9282 7.11977 14.8 7.24801C14.6718 7.37624 14.4978 7.44828 14.3165 7.44828Z" fill=""/>
                <path d="M7.04364 15.0151C6.23154 15.0147 5.44469 14.7327 4.81718 14.2173C4.18966 13.7018 3.76031 12.9846 3.60227 12.1881C3.44423 11.3915 3.56729 10.5647 3.95048 9.84873C4.33367 9.13272 4.95328 8.57173 5.70373 8.26135C6.45418 7.95097 7.28904 7.91041 8.06604 8.14658C8.84304 8.38275 9.51412 8.88103 9.96491 9.55652C10.4157 10.232 10.6183 11.0429 10.5383 11.8511C10.4582 12.6592 10.1004 13.4146 9.52579 13.9885C9.20012 14.3147 8.81318 14.5734 8.3872 14.7496C7.96122 14.9258 7.50462 15.016 7.04364 15.0151ZM7.04364 9.36518C6.54795 9.36552 6.06771 9.53772 5.68475 9.85246C5.30179 10.1672 5.03981 10.605 4.94344 11.0912C4.84708 11.5774 4.9223 12.0821 5.15628 12.5191C5.39027 12.956 5.76854 13.2984 6.22664 13.4878C6.68474 13.6771 7.19433 13.7018 7.66857 13.5575C8.14281 13.4133 8.55237 13.1091 8.82745 12.6967C9.10253 12.2843 9.22612 11.7893 9.17715 11.2961C9.12818 10.8028 8.9097 10.3418 8.55891 9.99153C8.36004 9.79248 8.1238 9.63467 7.86376 9.52719C7.60373 9.4197 7.32502 9.36464 7.04364 9.36518Z" fill=""/>
                <path d="M10.7833 15.9295C10.6935 15.9295 10.6045 15.9118 10.5215 15.8775C10.4385 15.8432 10.3631 15.7928 10.2996 15.7293L8.55913 13.9884C8.43458 13.8594 8.36566 13.6867 8.36721 13.5074C8.36877 13.3281 8.44069 13.1566 8.56746 13.0299C8.69424 12.9031 8.86575 12.8312 9.04503 12.8296C9.22432 12.8281 9.39704 12.897 9.52601 13.0215L11.266 14.7625C11.3615 14.8581 11.4266 14.9799 11.4529 15.1124C11.4792 15.245 11.4657 15.3824 11.414 15.5073C11.3623 15.6321 11.2747 15.7389 11.1624 15.814C11.05 15.8892 10.918 15.9293 10.7828 15.9295H10.7833Z" fill=""/>
            </svg>
              Overview</h3>
          </div>
        ) : (
          <button
            className={styles.lender_dashboard_application_details_tab_btn}
            onClick={() => handleTabChange('OVERVIEW', '/')}
          >
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>
            <svg width="15" height="20" viewBox="0 0 15 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M12.7157 19.2149H2.2843C1.67869 19.2142 1.09809 18.9733 0.669856 18.545C0.241623 18.1168 0.000723841 17.5362 0 16.9306V3.17371C0.000723841 2.56809 0.241623 1.98749 0.669856 1.55926C1.09809 1.13103 1.67869 0.890128 2.2843 0.889404H9.12673C9.21654 0.889364 9.30548 0.907016 9.38847 0.941353C9.47145 0.975691 9.54687 1.02604 9.61039 1.08953L14.7999 6.28083C14.8634 6.34436 14.9137 6.41977 14.948 6.50276C14.9824 6.58575 15 6.67469 15 6.7645V16.9301C14.9994 17.5358 14.7586 18.1165 14.3303 18.5449C13.9021 18.9732 13.3214 19.2142 12.7157 19.2149ZM2.2843 2.25698C2.04125 2.25722 1.80821 2.35388 1.63634 2.52575C1.46448 2.69762 1.36781 2.93065 1.36757 3.17371V16.9306C1.36781 17.1736 1.46448 17.4067 1.63634 17.5785C1.80821 17.7504 2.04125 17.8471 2.2843 17.8473H12.7157C12.9588 17.8471 13.1918 17.7504 13.3637 17.5785C13.5355 17.4067 13.6322 17.1736 13.6324 16.9306V7.04758L8.84182 2.25698H2.2843Z" fill=""/>
                <path d="M14.3165 7.44828H10.7257C10.1201 7.44756 9.5395 7.20666 9.11126 6.77843C8.68303 6.35019 8.44213 5.76959 8.44141 5.16398V1.57319C8.44141 1.39184 8.51345 1.21792 8.64168 1.08968C8.76992 0.961446 8.94384 0.889404 9.12519 0.889404C9.30654 0.889404 9.48047 0.961446 9.6087 1.08968C9.73694 1.21792 9.80898 1.39184 9.80898 1.57319V5.16398C9.80922 5.40704 9.90588 5.64007 10.0777 5.81194C10.2496 5.98381 10.4827 6.08047 10.7257 6.08071H14.3165C14.4978 6.08071 14.6718 6.15275 14.8 6.28099C14.9282 6.40922 15.0003 6.58314 15.0003 6.7645C15.0003 6.94585 14.9282 7.11977 14.8 7.24801C14.6718 7.37624 14.4978 7.44828 14.3165 7.44828Z" fill=""/>
                <path d="M7.04364 15.0151C6.23154 15.0147 5.44469 14.7327 4.81718 14.2173C4.18966 13.7018 3.76031 12.9846 3.60227 12.1881C3.44423 11.3915 3.56729 10.5647 3.95048 9.84873C4.33367 9.13272 4.95328 8.57173 5.70373 8.26135C6.45418 7.95097 7.28904 7.91041 8.06604 8.14658C8.84304 8.38275 9.51412 8.88103 9.96491 9.55652C10.4157 10.232 10.6183 11.0429 10.5383 11.8511C10.4582 12.6592 10.1004 13.4146 9.52579 13.9885C9.20012 14.3147 8.81318 14.5734 8.3872 14.7496C7.96122 14.9258 7.50462 15.016 7.04364 15.0151ZM7.04364 9.36518C6.54795 9.36552 6.06771 9.53772 5.68475 9.85246C5.30179 10.1672 5.03981 10.605 4.94344 11.0912C4.84708 11.5774 4.9223 12.0821 5.15628 12.5191C5.39027 12.956 5.76854 13.2984 6.22664 13.4878C6.68474 13.6771 7.19433 13.7018 7.66857 13.5575C8.14281 13.4133 8.55237 13.1091 8.82745 12.6967C9.10253 12.2843 9.22612 11.7893 9.17715 11.2961C9.12818 10.8028 8.9097 10.3418 8.55891 9.99153C8.36004 9.79248 8.1238 9.63467 7.86376 9.52719C7.60373 9.4197 7.32502 9.36464 7.04364 9.36518Z" fill=""/>
                <path d="M10.7833 15.9295C10.6935 15.9295 10.6045 15.9118 10.5215 15.8775C10.4385 15.8432 10.3631 15.7928 10.2996 15.7293L8.55913 13.9884C8.43458 13.8594 8.36566 13.6867 8.36721 13.5074C8.36877 13.3281 8.44069 13.1566 8.56746 13.0299C8.69424 12.9031 8.86575 12.8312 9.04503 12.8296C9.22432 12.8281 9.39704 12.897 9.52601 13.0215L11.266 14.7625C11.3615 14.8581 11.4266 14.9799 11.4529 15.1124C11.4792 15.245 11.4657 15.3824 11.414 15.5073C11.3623 15.6321 11.2747 15.7389 11.1624 15.814C11.05 15.8892 10.918 15.9293 10.7828 15.9295H10.7833Z" fill=""/>
            </svg>
               Overview</h3>
          </button>
        )}
        {activeTab === 'FINANCIAL' ? (
          <div className={styles.application_tab_btn}>
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Financial</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('FINANCIAL', '/financial')}
          >
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Financial</h3>
          </button>
        )}

        {activeTab === 'CREDIT' ? (
          <div className={styles.application_tab_btn}>
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Credit</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('CREDIT', '/credit')}
          >
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Credit</h3>
          </button>
        )}
        {activeTab === 'REPORT' ? (
          <div className={styles.application_tab_btn}>
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Report</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('REPORT', '/report')}
          >
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Report</h3>
          </button>
        )}
        {activeTab === 'PROFILE' ? (
          <div className={styles.application_tab_btn}>
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Profile</h3>
          </div>
        ) : (
          <button
            className=" w-full h-10 flex justify-center items-center"
            onClick={() => handleTabChange('PROFILE', '/profile')}
          >
            <h3 className={styles.lender_dashboard_application_details_tab_btn_txt}>Profile</h3>
          </button>
        )}
      </div>

      <Outlet></Outlet>
    </div>


     /* ----<div className={styles['container']}>
      
        </div> ----*/
  );
}

export default MyApplication;
