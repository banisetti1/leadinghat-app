import styles from './Financial.module.scss';

import BankAcIcon from '../../../assets/finance-bank-ac-icon.svg';
import exportBtnIcon from '../../../assets/document-export-icon.svg';
import AcProfileImg from '../../../assets/ac-profile-img.svg';
import SuccessCheck from '../../../assets/checked-success.svg';
import InstituteIcon from '../../../assets/institute-icon.svg';
import BalanceGraph from '../../../assets/balance-graph.svg';
import transactionIcon from '../../../assets/transaction-icon.svg';


import {Text, Button} from '@lendinghat/common-ui';

/* eslint-disable-next-line */
export interface FinancialProps {}

export function Financial(props: FinancialProps) {
  return (
    <div>

      <div className={styles.lender_dashboard_finance_bank_details}>
        <div className={styles.lender_dashboard_finance_bank_details_wrapper}>
          <div className={styles.lh_card_wrapper_justify_between}>
            <div className={styles.section_icon_text}>
              <div className={styles.section_icon}>
                <img src={BankAcIcon}/>
              </div>
              <p className={styles.p_black}>Bank Account Details</p>
            </div>

            <div className={styles.lh_card_wrapper_flex_col}>
              <p className={styles.section_subheading}>Account Number</p>
              <Text as={`p`}>4003830171874018</Text>
            </div>
          </div>

          <div className={styles.lender_dashboard_application_loan_summary}>  
          <div className={styles.lender_dashboard_application_loan_summary_data}>
            <div className={styles.lender_dashboard_application_loan_summary_total}>
              <ul className={styles.account_details_list}>
                <li>
                  <img src={AcProfileImg}/>
                    <div className={styles.account_details_item}>
                      <p className={styles.p_subtitle}>Account Name</p>
                      <Text as={`h5`}>Michael Manley <img src={SuccessCheck}/></Text>
                    </div>
                </li>

                <li>
                  <img src={InstituteIcon}/>
                    <div className={styles.account_details_item}>
                      <p className={styles.p_subtitle}>Institution Name</p>
                      <Text as={`h5`}>JPMorgan Chase</Text>
                    </div>
                </li>

              </ul>
            </div>

            <div className={styles.lender_dashboard_application_loan_summary_graph}>
              <div className={styles.lender_dashboard_application_loan_summary_graph_content}>

                <ul className={styles.balance_list}>
                  <li>
                    <p className={styles.p_subtitle}>Balance</p>
                    <Text as={`h5`}>$5,000</Text>
                  </li>

                  <li>
                    <p className={styles.p_subtitle}>AC Type:<span> Current</span></p>
                  </li>
                </ul>
                <img src={BalanceGraph} />
              </div>
            </div>

            <div className={styles.lender_dashboard_application_loan_summary_type_info}>
            <ul className={styles.account_id_list}>
                  <li>
                    <p className={styles.p_subtitle}>account ID</p>
                    <p className={styles.p_title}>021000021</p>
                  </li>

                  <li>
                    <p className={styles.p_subtitle}>Institution ID</p>
                    <p className={styles.p_title}>A001</p>
                  </li>
                </ul>
            </div>
          </div>        
          </div>
        </div>

      </div>

      <div className={styles.lender_dashboard_finance_transaction}>
        <div className={styles.lender_dashboard_finance_transaction_wrapper}>
          <div className={styles.lh_card_wrapper_justify_between}>
              <div className={styles.lender_dashboard_finance_transaction_title}>
                <div className={styles.lender_dashboard_finance_transaction_title_icon}>
                  <img src={transactionIcon}/>
                </div>
                <Text as={`p_black`}>Transactions</Text>
              </div>

              <div className={styles.lender_dashboard_finance_transaction_filter}>
                <input type='date' className={styles.input_field_type_b_dark}/>
                
                <select className={styles.select_dropdown}>
                  <option>Category</option>
                  <option>Category</option>
                  <option>Category</option>
                  <option>Category</option>
                </select>

                <select className={styles.select_transaction_type}>
                  <option>Category</option>
                  <option>Category</option>
                  <option>Category</option>
                  <option>Category</option>
                </select>
             </div>
          </div>

          <div className={styles.lender_dashboard_finance_institution}>
            <p>Institution name: JPMorgan Chase</p>
          </div>

          <div className={styles.transaction_list_table_wrapper}>
            <table className="table-fixed">
    <thead>
      <tr>
        <th>Transaction id</th>
        <th>Date </th>
        <th>Merchant</th>
        <th> Category</th>
        <th>Recurring Interval  </th>
        <th>Recurring End Date</th>
        <th>Transaction Type</th>
        <th>Amount</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>#34364856</td>
        <td>Jun 20, 2023</td>
        <td className={styles.merchant_status}><span className={styles.status_yellow}>AH</span>arc Trade Holding Ltd, USA</td>
        <td>Single Transactions</td>
        <td>-</td>
        <td>-</td>
        <td>internet banking</td>
        <td>$25,45,735</td>
      </tr>

      <tr>
        <td>#34364856</td>
        <td>Jun 20, 2023</td>
        <td className={styles.merchant_status}><span className={styles.status_magenta}>AH</span>arc Trade Holding Ltd, USA</td>
        <td>Single Transactions</td>
        <td>-</td>
        <td>-</td>
        <td>internet banking</td>
        <td>$25,45,735</td>
      </tr>

      <tr>
        <td>#34364856</td>
        <td>Jun 20, 2023</td>
        <td className={styles.merchant_status}><span className={styles.status_blue}>AH</span>arc Trade Holding Ltd, USA</td>
        <td>Single Transactions</td>
        <td>-</td>
        <td>-</td>
        <td>internet banking</td>
        <td>$25,45,735</td>
      </tr>

      <tr>
        <td>#34364856</td>
        <td>Jun 20, 2023</td>
        <td className={styles.merchant_status}><span className={styles.status_success}>AH</span>arc Trade Holding Ltd, USA</td>
        <td>Single Transactions</td>
        <td>-</td>
        <td>-</td>
        <td>internet banking</td>
        <td>$25,45,735</td>
      </tr>

      <tr>
        <td>#34364856</td>
        <td>Jun 20, 2023</td>
        <td className={styles.merchant_status}><span className={styles.status_orange}>AH</span>arc Trade Holding Ltd, USA</td>
        <td>Single Transactions</td>
        <td>-</td>
        <td>-</td>
        <td>internet banking</td>
        <td>$25,45,735</td>
      </tr>

    </tbody>
            </table>
          </div>
        </div>
      </div>

    </div>
  );
}

export default Financial;
