import { render } from '@testing-library/react';

import MyApplication from './MyApplications';

describe('MyApplication', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MyApplication />);
    expect(baseElement).toBeTruthy();
  });
});
