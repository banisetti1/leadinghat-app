import styles from './Report.module.scss';

/* eslint-disable-next-line */
export interface ReportProps {}

export function Report(props: ReportProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to Report!</h1>
    </div>
  );
}

export default Report;
