import styles from './AnalyticsPage.module.scss';

import totalFundBg from '../../assets/analytics-total-fund-img.svg';
import totalCompBg from '../../assets/analytics-total-com-img.svg';
import exportBtnIcon from '../../assets/document-export-icon.svg';
import industryFundChart from '../../assets/industries-fund-chart.svg';
import applicationSourceGraph from '../../assets/application-source-graph.svg';
import daycaresGraph from '../../assets/daycares-graph.svg';
import dumpTrucksGraph from '../../assets/dump-trucks-graph.svg';
import nonProfitGraph from '../../assets/nonprofit-graph.svg';
import beautySalonGraph from '../../assets/beauty-salon-graph.svg';
import lawFirmGraph from '../../assets/law-firm-graph.svg';
import autoRepairGraph from '../../assets/auto-repair-graph.svg';
import dealStatisticsGraph from '../../assets/deals-statistics-graph.svg';

import {Text, Card, Button} from '@lendinghat/common-ui';

/* eslint-disable-next-line */
export interface AnalyticsProps {}

export function Analytics(props: AnalyticsProps) {
  return (
    <div>
      <div className={styles.lender_dashboard_route_title}>
        <Text as={`h5`}>Analytics</Text>
        <a href='#' className={styles.data_export_btn}>
          <img src={exportBtnIcon} />
        </a>
        
      </div>

      <div className={styles.lender_dashboard_feature}>
        <div className={styles.lender_dashboard_feature_60}>
          <div className={styles.lh_card_light_b_shadow}>
            <div className={styles.lender_dashboard_feature_60__title}>
              <Text as={`p_black`}>Total Funded Amount</Text>

              <input
                type="date"
                className={styles.input_field_type_b_dark}
                placeholder="last 6 months"
              />
            </div>
            <img src={totalFundBg} className=''/>
          </div>
        </div>
        <div className={styles.lender_dashboard_feature_40}>
        <div className={styles.lh_card_light_b_shadow}>
        <div className={styles.lender_dashboard_feature_60__title}>
              <Text as={`p_black`}>Industries applied</Text>

              <input
                type="date"
                className={styles.input_field_type_b_dark}
                placeholder="last 6 months"
              />
            </div>
            <img src={totalCompBg} className=''/>

            <ul className={styles.lh_card_content_grid_II}>
              <li><span className={styles.medical_graph}></span><p className={styles.graph_title}>Medical practices</p></li>
              <li><span className={styles.vicle_graph}></span><p className={styles.graph_title}>Commercial vehicles</p></li>
              <li><span className={styles.construction_graph}></span><p className={styles.graph_title}>Construction</p></li>
              <li><span className={styles.restaurant_graph}></span><p className={styles.graph_title}>Restaurants</p></li>
            </ul>
        </div>
        </div>
      </div>


      <div className={styles.lender_dashboard_feature}>
        <div className={styles.lender_dashboard_feature_30}>
            <div className={styles.lh_card_light_b_shadow}>
              <Text as={`p_black`}>Industries Funded</Text>

              <input
                type="date"
                className={styles.input_field_type_b_dark}
                placeholder="last 6 months"
              />

              <img src={industryFundChart} className={styles.industry_fund_chart}/>

              <ul className={styles.lh_card_content_grid_I}>
                  <li><span className={styles.medical_graph}></span><p className={styles.graph_title}>Medical practices</p></li>
                  <li><span className={styles.vicle_graph}></span><p className={styles.graph_title}>Commercial vehicles</p></li>
                  <li><span className={styles.construction_graph}></span><p className={styles.graph_title}>Construction</p></li>
                  <li><span className={styles.restaurant_graph}></span><p className={styles.graph_title}>Restaurants</p></li>
                  <li><span className={styles.Farm_equipment_graph}></span><p className={styles.graph_title}>Farm equipment: 15%</p></li>
                  <li><span className={styles.gas_station_graph}></span><p className={styles.graph_title}>gas stations: 08%</p></li>
                  <li><span className={styles.medical_equipment_graph}></span><p className={styles.graph_title}>Medical equipment: 15%</p></li>
              </ul>

            </div>
        </div>

        <div className={styles.lender_dashboard_feature_70}>
          <div className={styles.card_content}>
            <div className={styles.lh_card_wrapper_justify_between}>
                <Text as={`p_black`}>Application sources</Text>

                <input
                    type="date"
                    className={styles.input_field_type_b_dark}
                    placeholder="last 6 months"
                  />
             </div>

             <img src={applicationSourceGraph} className='' />

            <ul className={styles.lh_card_content_grid_III}>
              <li><span className={styles.lh_lending_graph}></span><p>Lending Hat</p></li>
              <li><span className={styles.lh_yellow}></span><p>Merchant</p></li>
              <li><span className={styles.lh_magenta}></span><p>Broker</p></li>
            </ul>
          </div>
          

            <div className={styles.least_applied_industries}>
              <div className={styles.lh_card_wrapper_justify_between}>
                <Text as={`p_black`}>Least applied industries</Text>

                <input
                    type="date"
                    className={styles.input_field_type_b_dark}
                    placeholder="last 6 months"
                  />
              </div>

              <div className={styles.lh_card_wrapper}>
                <div className={styles.lh_card_light_shadow}>
                  <div className={styles.lh_card_data}>
                    <p className={styles.p}>Daycares</p>
                    <h5 className={styles.h5}>45</h5>
                  </div>

                  <div className={styles.lh_card_data}>
                    <p className={styles.indicate_loose}>15%</p>

                    <img src={daycaresGraph} />
                  </div>
                </div>

                <div className={styles.lh_card_light_shadow}>
                  <div className={styles.lh_card_data}>
                    <p className={styles.p}>dump trucks</p>
                    <h5 className={styles.h5}>23</h5>
                  </div>

                  <div className={styles.lh_card_data}>
                    <p className={styles.indicate_profit}>10%</p>

                    <img src={dumpTrucksGraph} />
                  </div>
                </div>

                <div className={styles.lh_card_light_shadow}>
                  <div className={styles.lh_card_data}>
                    <p className={styles.p}>Nonprofits</p>
                    <h5 className={styles.h5}>10</h5>
                  </div>

                  <div className={styles.lh_card_data}>
                    <p className={styles.indicate_profit}>12%</p>

                    <img src={nonProfitGraph} />
                  </div>
                </div>

                <div className={styles.lh_card_light_shadow}>
                  <div className={styles.lh_card_data}>
                    <p className={styles.p}>Beauty salons</p>
                    <h5 className={styles.h5}>21</h5>
                  </div>

                  <div className={styles.lh_card_data}>
                    <p className={styles.indicate_profit}>17%</p>

                    <img src={beautySalonGraph} />
                  </div>
                </div>

                <div className={styles.lh_card_light_shadow}>
                  <div className={styles.lh_card_data}>
                    <p className={styles.p}>law firms</p>
                    <h5 className={styles.h5}>05</h5>
                  </div>

                  <div className={styles.lh_card_data}>
                    <p className={styles.indicate_profit}>08%</p>

                    <img src={lawFirmGraph} />
                  </div>
                </div>

                <div className={styles.lh_card_light_shadow}>
                  <div className={styles.lh_card_data}>
                    <p className={styles.p}>Auto Repair</p>
                    <h5 className={styles.h5}>15</h5>
                  </div>

                  <div className={styles.lh_card_data}>
                    <p className={styles.indicate_loose}>05%</p>

                    <img src={autoRepairGraph} />
                  </div>
                </div>       
                
              </div>
            </div>

        </div>
      </div>

      <div className={styles.lender_dashboard_feature_full}>
        <div className={styles.lh_card_light_b}>
            <div className={styles.lh_card_wrapper_justify_between}>
              <Text as={`p_black`}>Deals statistics</Text>
              <div className={styles.lh_card_wrapper_flex_center}>
                <ul className={styles.lh_card_content_grid_II}>
                  <li><span className={styles.inprogress_deals_graph}></span><p>In-progress Deals</p></li>
                  <li><span className={styles.close_deals_graph}></span><p>Closed Deals</p></li>
                </ul>
              <input type="date" className={styles.input_field_type_b_dark} placeholder="last 6 months"/>
              </div>
           </div>

           <img src={dealStatisticsGraph} />
        </div>
      </div>
        
    </div>
  );
}

export default Analytics;
