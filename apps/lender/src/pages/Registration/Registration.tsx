import styles from './Registration.module.scss';

/* eslint-disable-next-line */
export interface RegistrationProps {}

export function Registration(props: RegistrationProps) {
  return (
    <div className={styles['container']}>
      <h1 className={styles.lh_heading}>Welcome Back</h1>
      <p className={styles.lh_subheading}>Log into your account</p>
      <div className={styles.lh_blog_post_data}>
        <form>
          <div className="mb-6">
            <input
              type="email"
              id="email"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-12 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Work Email Address"
              required
            />
          </div>
          <div className="mb-6">
            <input
              type="password"
              id="password"
              className="bg-white border border-lh-border-color text-gray-900 text-sm rounded-md focus:ring-blue-500 focus:border-blue-500 block h-12 w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
              placeholder="Password"
              required
            />
          </div>
          <div className="flex items-start mb-6">
            <div className="flex items-center h-5">
              <input
                id="remember"
                type="checkbox"
                value=""
                className="w-4 h-4 border border-lh-border-color rounded bg-gray-50 focus:ring-3 focus:ring-blue-300 dark:bg-gray-700 dark:border-gray-600 dark:focus:ring-blue-600 dark:ring-offset-gray-800 dark:focus:ring-offset-gray-800"
                required
              />
            </div>
            <label
              htmlFor="remember"
              className="flex flex-1 w-fit ml-2 text-sm font-medium text-gray-900 dark:text-gray-300"
            >
              Remember me
            </label>
            <a href="/forgot" className="text-blue-500 text-sm font-normal">
              Forgot Password?
            </a>
          </div>

          <button
            type="submit"
            className={`${styles.lh_u_btn} ${styles.lh_u_btn__solid} ${styles.lh_u_btn__solid__icon} ${styles.lh_u_btn__rounded} w-full`}
          >
            Login
          </button>
          <div className="flex flex-row mt-7 justify-center items-center">
            <label htmlFor="remember" className="px-3">
              Are you new to the Lending Hat?
            </label>
            <a href="/forgot" className="text-blue-500 text-sm font-bold">
              Schedule A Demo
            </a>
          </div>
        </form>
      </div>
    </div>
  );
}

export default Registration;
