import styles from './MerchantRegistration.module.scss';
import { Container, TextInput } from '@lendinghat/common-ui';

/* eslint-disable-next-line */
export interface MerchantRegistrationProps {}

export function MerchantRegistration(props: MerchantRegistrationProps) {
  return (
    <div>
      <section className={styles.lh_merchant_reg_rate}>
        <Container style={styles.merchant_reg_rate_container as any}>
          <div className="lh_merchant_reg_rate_form_area">
            <h3 className={styles.h3}>Check Your Rate</h3>
            <p className={styles.p}>
              This is a form to check your Credit Rate by providing the
              following information and register with us
            </p>
            <form action="" className={styles.lh_merchant_reg_rate_form}>
              <div className={styles.form_checkbox_group}>
                <p>Number of applicants:</p>

                <div className={styles.form_checkbox_filed}>
                  <input type="checkbox" id="form_check_self" />
                  <label htmlFor="form_check_self">Self</label>
                </div>

                <div className={styles.form_checkbox_filed}>
                  <input type="checkbox" id="form_check_joint" />
                  <label htmlFor="form_check_joint">Joint</label>
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_filed}>
                  <label htmlFor="test" className={styles.label}>
                    Applicant’s Name
                  </label>
                  <input
                    type="text"
                    id="test"
                    className={styles.inputtype_I}
                    placeholder=""
                  />
                </div>

                <div className={styles.form_group_filed}>
                  <label htmlFor="gmail" className={styles.label}>
                    Email Address
                  </label>
                  <input
                    type="email"
                    id="gmail"
                    className={styles.inputtype_I}
                    placeholder=""
                  />
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_filed}>
                  <label htmlFor="date" className={styles.label}>
                    Date of Barth
                  </label>
                  <input
                    type="date"
                    name="begin"
                    placeholder="dd-mm-yyyy"
                    className={styles.inputtype_I}
                    id="date"
                    value=""
                    min="1997-01-01"
                    max="2030-12-31"
                  />
                </div>
                <div className={styles.form_group_filed}>
                  <label htmlFor="tax" className={styles.label}>
                    Tax ID number
                  </label>
                  <input type="text" className={styles.inputtype_I} id="tax" />
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_filed}>
                  <label htmlFor="select_income" className={styles.label}>
                    Monthly Income
                  </label>
                  <select className={styles.inputtype_I} id="select_income">
                    <option>Select Option</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>

                <div className={styles.form_group_filed}>
                  <label htmlFor="select_income" className={styles.label}>
                    Total Annual Income
                  </label>
                  <input
                    type="text"
                    placeholder=""
                    className={styles.inputtype_I}
                  />
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_filed}>
                  <label htmlFor="range" className={styles.label}>
                    Total Loan Amount
                  </label>
                  <input
                    type="range"
                    id="range"
                    className={styles.inputtype_I}
                  />
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_filed}>
                  <label htmlFor="loan" className={styles.label}>
                    Purpose Loan
                  </label>
                  <select id="loan" className={styles.inputtype_I}>
                    <option>Purpose Loan</option>
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                  </select>
                </div>

                <div className={styles.form_group_filed}>
                  <label htmlFor="address" className={styles.label}>
                    Home Address
                  </label>
                  <input
                    type="text"
                    id="address"
                    className={styles.inputtype_I}
                  />
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_filed}>
                  <label htmlFor="password" className={styles.label}>
                    Password
                  </label>
                  <input
                    type="password"
                    id="password"
                    className={styles.inputtype_I}
                  />
                </div>

                <div className={styles.form_group_filed}>
                  <label htmlFor="confirm_password" className={styles.label}>
                    Confirm Password
                  </label>
                  <input
                    type="password"
                    id="confirm_pass"
                    className={styles.inputtype_I}
                  />
                </div>
              </div>

              <div className={styles.form_group}>
                <div className={styles.form_group_submit}>
                  <input
                    type="checkbox"
                    className="check_submission"
                    id="submit_confirmation"
                  />
                  <label htmlFor="submit_confirmation"></label>
                  <p>
                    Yes, I would like to receive lending hat marketing
                    communications.
                  </p>
                </div>
              </div>

              <div className={styles.form_group}>
                <p className="">
                  By signing up, you agree to our{' '}
                  <a href="/" className={styles.para_link}>
                    T&Cs,
                  </a>
                  Privacy Policy, ENSIGN Act Consent & Credit Profile
                  Authorisation
                </p>
              </div>
            </form>
          </div>
        </Container>
      </section>

      <section className={styles.lh_merchant_copyright}>
        <Container>
          <p className={styles.lh_merchant_copyright_para}>
            Copyright 2022-23 Lending Hat | All Rights Reserved
          </p>
        </Container>
      </section>
    </div>
  );
}

export default MerchantRegistration;
