import * as React from 'react';
import ReactDOM from 'react-dom/client';

import {
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  useReactTable,
} from '@tanstack/react-table';

type Application = {
  applicationId: string;
  loanType: string;
  loanSource: string;
  amount: string;
  merchant: string;
  industry: string;
  revenue: number;
  date: number;
  status: boolean;
};

const defaultData: Application[] = [
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
];

const columnHelper = createColumnHelper<Application>();

const columns = [
  columnHelper.accessor('applicationId', {
    cell: (info) => info.getValue(),
  }),
  columnHelper.accessor((row) => row.loanType, {
    id: 'loanType',
    cell: (info) => <i>{info.getValue()}</i>,
    header: () => <span>Loan Type</span>,
  }),
  columnHelper.accessor('loanSource', {
    header: () => 'Loan Source',
    cell: (info) => info.renderValue(),
  }),
  columnHelper.accessor('amount', {
    header: () => <span>Amount</span>,
  }),
  columnHelper.accessor('merchant', {
    header: 'Merchant',
  }),
  columnHelper.accessor('industry', {
    header: 'Industry',
  }),
  columnHelper.accessor('revenue', {
    header: 'Revenue',
  }),
  columnHelper.accessor('date', {
    header: 'Date',
  }),
  columnHelper.accessor('status', {
    header: 'Status',
  }),
];

function Table() {
  const [data, setData] = React.useState(() => [...defaultData]);
  const rerender = React.useReducer(() => ({}), {})[1];

  const table = useReactTable({
    data,
    columns,
    getCoreRowModel: getCoreRowModel(),
  });

  return (
    <div className="p-2">
      <table>
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map((row) => (
            <tr key={row.id}>
              {row.getVisibleCells().map((cell) => (
                <td key={cell.id}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
        <tfoot>
          {table.getFooterGroups().map((footerGroup) => (
            <tr key={footerGroup.id}>
              {footerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.footer,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </tfoot>
      </table>
    </div>
  );
}

export default Table;
