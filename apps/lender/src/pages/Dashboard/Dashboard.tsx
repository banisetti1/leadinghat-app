import styles from './Dashboard.module.scss';

import AppliedLoanGraph from '../../assets/applied-loan-graph-icon.svg';
import CardDarkBg from '../../assets/card-dark-bg.svg';
import AppOverviewIcon from '../../assets/lender-application-icon.svg';
import AppOverviewGraphBg from '../../assets/overview-graph-bg.svg';
import SelectDropDown from '../../assets/select-dropdown.svg';
import ToggleIcon from '../../assets/toggle-icon.svg';
import { formatDistanceToNow } from 'date-fns';

import {
  Text,
  Button,
  Table,
  RDatePicker as DatePicker,
} from '@lendinghat/common-ui';
import { useState } from 'react';
/* eslint-disable-next-line */
export interface DashboardProps {}

export function Dashboard(props: DashboardProps) {
  const [seletedDate, setSelectedDate] = useState<Date | null>(
    new Date(2023, 3, 21)
  );

  return (
    <div>
      <div className={styles.lender_dashboard_route_title}>
        <Text as={`h5`}>Dashboard</Text>
      </div>

      <div className={styles.lender_dashboard_loan_apply}>
        <div className={styles.lh_card_light_b_shadow}>
          <div className={styles.lh_card_wrapper_justify_baseline}>
            <div className="lh_card_content">
              <Text as={`p_black`}>Applied Loans</Text>
              <h4 className={styles.lender_dashboard_loan_apply_ammount_black}>
                $15,254,000
              </h4>
              <p className={styles.lender_dashboard_loan_apply_ammount_para}>
                Industry Preference: <span>Construction</span>
              </p>
            </div>

            <div className="lh_card_content">
              <DatePicker
                placeholder={`last ${formatDistanceToNow(seletedDate as any)}`}
                className={styles.input_field_type_b_dark}
                selected={seletedDate}
                placeholderStyle={'pr-[15px]'}
                onChange={(date: Date | null) => setSelectedDate(date)}
              />

              <img src={AppliedLoanGraph} />
            </div>
          </div>
        </div>

        <div className={styles.lh_card_dark}>
          <div className={styles.lh_card_wrapper_justify_baseline}>
            <div className="lh_card_content">
              <Text as={`p_light`}>Approved</Text>
              <h4 className={styles.lender_dashboard_loan_apply_ammount_light}>
                $5,678,000
              </h4>
            </div>

            <div className="lh_card_content">
              <DatePicker
                dark={true}
                placeholder={`last ${formatDistanceToNow(seletedDate as any)}`}
                className={styles.input_field_type_b_light}
                selected={seletedDate}
                onChange={(date: Date | null) => setSelectedDate(date)}
              />
            </div>
          </div>
        </div>
      </div>

      <div className={styles.lender_dashboard_loan_overview}>
        <div className={styles.lender_dashboard_loan_overview_left_panel}>
          <div
            className={
              styles.lender_dashboard_loan_overview_left_panel_inner_content
            }
          >
            <div className={styles.lh_card_content_heading}>
              <Text as={`p_black`}>Loan Overview</Text>
            </div>

            <div className="lh_card_content">
              <p
                className={styles.lender_dashboard_loan_overview_left_subtitle}
              >
                You have
              </p>
              <h1 className={styles.lender_dashboard_loan_overview_left_title}>
                05
              </h1>
              <p
                className={styles.lender_dashboard_loan_overview_left_subtitle}
              >
                New Applications
              </p>
            </div>

            <ul className={styles.lh_card_content_grid_II}>
              <li>
                <span className={styles.lh_magenta}></span>
                <p>
                  Completed<span>145</span>
                </p>
              </li>

              <li>
                <span className={styles.lh_yellow}></span>
                <p>
                  Active<span>30</span>
                </p>
              </li>

              <li>
                <span className={styles.lh_blue}></span>
                <p>
                  Default<span>12</span>
                </p>
              </li>
            </ul>
          </div>

          <div
            className={
              styles.lender_dashboard_loan_overview_left_panel_inner_content_last
            }
          >
            <input type="date" className={styles.input_field_type_b_dark} />
            <DatePicker
                placeholder={`last ${formatDistanceToNow(seletedDate as any)}`}
                className={styles.input_field_type_b_dark}
                selected={seletedDate}
                placeholderStyle={'pr-[15px] text-[0.625rem] font-normal'}
                onChange={(date: Date | null) => setSelectedDate(date)}
              />
            <img src={AppOverviewGraphBg} />

            <ul className={styles.lh_card_content_grid_III}>
              <li><span className={styles.lh_magenta}></span><p>Completed</p></li>

              <li><span className={styles.lh_yellow}></span><p>Active</p></li>

              <li><span className={styles.lh_blue}></span><p>Default</p></li>
            </ul>
          </div>

          <a href="#" className={styles.panel_toggle}>
            <img src={ToggleIcon} />
          </a>

          
        </div>

        <div className={styles.lender_dashboard_loan_overview_right_panel}>
          <div
            className={
              styles.lender_dashboard_loan_overview_right_inner_content
            }
          >
            <div className={styles.lh_card_content}>
              <Text as={`p_black`}>Applications Overview</Text>
              <Button
                label="View All"
                variant="link"
                className={styles.lender_dashboard_link_btn}
              />
            </div>

            <div
              className={
                styles.lender_dashboard_loan_overview_right_inner_content_data_area
              }
            >
              <div
                className={
                  styles.lender_dashboard_loan_overview_right_card_light
                }
              >
                <p
                  className={
                    styles.lender_dashboard_loan_overview_right_card_title
                  }
                >
                  Applications
                </p>
                <p
                  className={
                    styles.lender_dashboard_loan_overview_right_card_num
                  }
                >
                  302
                </p>
              </div>
              <img src={AppOverviewIcon} />
            </div>

            <div
              className={
                styles.lender_dashboard_loan_overview_right_inner_content_data_area
              }
            >
              <ul className={styles.lh_card_content_grid_II}>
                <li>
                  <span className={styles.lh_magenta}></span>
                  <p>
                    Needs Review<span>105</span>
                  </p>
                </li>

                <li>
                  <span className={styles.lh_yellow}></span>
                  <p>
                    Approved<span>97</span>
                  </p>
                </li>

                <li>
                  <span className={styles.lh_blue}></span>
                  <p>
                    Declined<span>100</span>
                  </p>
                </li>
              </ul>
            </div>
          </div>

          <div
            className={styles.lender_dashboard_loan_overview_right_panel_graph}
          >
            <DatePicker
              placeholder={`last ${formatDistanceToNow(seletedDate as any)}`}
              className={styles.input_field_type_b_dark}
              selected={seletedDate}
              onChange={(date: Date | null) => setSelectedDate(date)}
            />

            <img src={AppOverviewGraphBg} />

            <ul className={styles.lh_card_content_grid_III}>
              <li>
                <span className={styles.lh_magenta}></span>
                <p>Need to review</p>
              </li>

              <li>
                <span className={styles.lh_yellow}></span>
                <p>Approved</p>
              </li>

              <li>
                <span className={styles.lh_blue}></span>
                <p>Declined</p>
              </li>
            </ul>
          </div>
        </div>
      </div>

      <div className={styles.lender_dashboard_application}>
        <div className={styles.lender_dashboard_application_title}>
          <Text as={`p_black`}>Applications</Text>
          <div className={styles.lender_dashboard_application_filter}>
            <div className={styles.lender_dashboard_application_filter_label}>
              <label htmlFor="loan_source">Filter by: </label>
              <select id="loan_source" className={styles.select_dropdown}>
                <option>Loan Source</option>
                <option>All</option>
                <option>Lending Hat</option>
                <option>Merchant</option>
                <option>Broker</option>
              </select>
            </div>
            <select className={styles.select_dropdown_industry}>
              <option>Industry</option>
              <option>Medical practices</option>
              <option>Farm equipment</option>
              <option>Construction</option>
              <option>Commercial vehicles</option>
              <option>Medical equipment</option>
            </select>
            <select className={styles.select_dropdown}>
              <option>Offer Status</option>
              <option>All</option>
              <option>Need Review</option>
              <option>Approved</option>
              <option>Declined</option>
            </select>

            <Button
              label="Underwrite All"
              variant="primary"
              className={styles.lender_dashboard_application_btn}
            />
          </div>
        </div>

        <div className={styles.lender_dashboard_application_table_wrapper}>
          <Table />
        </div>
      </div>
    </div>
  );
}

export default Dashboard;
