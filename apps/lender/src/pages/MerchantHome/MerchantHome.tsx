import styles from './MerchantHome.module.scss';
import { Container } from '@lendinghat/common-ui';

import { SecureIcon } from '@lendinghat/icons';
import HeroImg from '../../assets/lh_merchant_hero_icon.svg';

/* eslint-disable-next-line */
export interface MerchantHomeProps {}

export function MerchantHome(props: MerchantHomeProps) {
  return (
    <div>
      <section className={styles.lh_merchant_hero}>
        <Container style={styles.test_style as any}>
          <div className={styles.lh_merchant_hero_content}>
            <h1 className={styles.h1}>
              An all-in-one small business lending platform.
            </h1>
            <p className={styles.p}>
              Simple, smarter technology for higher approval
            </p>
            <div className={styles.lh_merchant_hero_btn_group}></div>
            <p className={styles.lh_merchant_hero_content_para}>
              <SecureIcon />
              Your information is secure with us
            </p>
          </div>

          <div className={styles.lh_merchant_hero_img}>
            <img src={HeroImg} />
          </div>
        </Container>
      </section>
    </div>
  );
}

export default MerchantHome;
