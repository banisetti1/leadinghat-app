import { render } from '@testing-library/react';

import MerchantHome from './MerchantHome';

describe('MerchantHome', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<MerchantHome />);
    expect(baseElement).toBeTruthy();
  });
});
