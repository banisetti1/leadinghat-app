import { Link, useNavigate } from 'react-router-dom';

import styles from './Login.module.scss';
import { useAppDispatch } from '../../app/store/hooks';

import Header from '../../layouts/Headers/Headers/Headers';

import { Button, Card } from '@lendinghat/common-ui';

import logoUrl from '../../assets/logo.png';
import { loginUser } from '../../app/store/slices/auth/auth.slice';
import BtnNxtArrow from '../../assets/nxt-arrow.svg';

/* eslint-disable-next-line */
export interface LoginProps {}

export function Login(props: LoginProps) {
  const dispatch = useAppDispatch();
  const navigate = useNavigate();
  const handleSubmit = (e: React.SyntheticEvent) => {
    e.preventDefault();
    dispatch(
      loginUser({
        email: 'eve.holt@reqres.in',
        password: 'cityslicka',
      })
    ).then((res) => {
      if (res?.payload?.token) {
        navigate('/dashboard', { replace: true });
      }
    });
  };
  return (
    <div>
      <Header />
      <main>
        <div className={styles.lender_login_section}>
          <h1 className={styles.lender_login_section__heading}>Welcome Back</h1>
          <p className={styles.lender_login_section__para}>
            Log into your account
          </p>

          <div className={styles.lender_login_panel}>
            <form className={styles.lender_login__form}>
              <div className={styles.lender_login_form_group}>
                <input
                  type="email"
                  className="lender_login_mail input_field_type_I"
                  id=""
                  placeholder="Work Email Address*"
                />
              </div>

              <div className={styles.lender_login_form_group}>
                <input
                  type="password"
                  className="lender_login_pass input_field_type_I"
                  id=""
                  placeholder="Password*"
                />
              </div>

              <div className={styles.lender_login_form_group}>
                <div className={styles.lender_login_input}>
                  <input
                    type="checkbox"
                    className="lender_login_checkbox"
                    id="lender_login_check"
                  />
                  <label
                    htmlFor="lender_login_check"
                    className={styles.lender_login_label}
                  >
                    Remember Me
                  </label>
                </div>

                <div className="lender_login_input">
                  <Link to="/forgot" className={styles.lender_login_pass_reset}>
                    Forget Password?
                  </Link>
                </div>
              </div>
              <Button
                label="Login"
                onClick={handleSubmit}
                variant="primary"
                className={styles.lender_login_panel_btn}
              />

              <div className={styles.lender_login_form_group}>
                <div className={styles.lender_login_input}>
                  <a
                    href="#"
                    className={`${styles.lender_login_input} ${styles.text_black}`}
                  >
                    Are you new to the Landing Hat?
                  </a>
                </div>

                <div className={styles.lender_login_input}>
                  <a
                    href="#"
                    className={`${styles.lender_login_pass_reset} ${styles.text_link}`}
                  >
                    Schedule A Demo <img src={BtnNxtArrow} className="" />
                  </a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </main>

      <div className={styles.lh_copyright}>
        <p className={styles.lh_copyright_para}>
          Copyright 2022-23 Lending Hat | All Rights Reserved
        </p>
      </div>
    </div>
  );
}

export default Login;
