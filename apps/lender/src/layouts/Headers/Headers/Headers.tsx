import styles from './Headers.module.scss';

import { Button } from '@lendinghat/common-ui';

import LogoUrl from '../../../assets/Lendinghat-logo.svg';

/* eslint-disable-next-line */
export interface HeadersProps {}

export function Headers(props: HeadersProps) {
  return (
    <div>
      <header className={styles.lender_header}>
        <div className={styles.lender_header__brand}>
          <img src={LogoUrl} className="" />
        </div>

        <ul className={styles.lender_header__option_list}>
          <li>
            <a href="#">Are you new to the Lending Hat? </a>
          </li>
          <li>
            <Button
              label="Schedule A Demo"
              onClick={() => {}}
              variant="primary"
              className={styles.lender_header__option_btn}
            />
          </li>
        </ul>
      </header>
    </div>
  );
}

export default Headers;
