import styles from './DashboardHeader.module.scss';

import sidebarClose from '../../assets/sidebar-closebtn-icon.svg';
import searchIcon from '../../assets/search-icon.svg';
import dashboardSettingIcon from '../../assets/dashboard-header-setting-icon.svg';
import dashboardNotificationIcon from '../../assets/dashboard-header-notification-icon.svg';
import sidebarOpen from '../../assets/sidebar-openbtn-icon.svg';


/* eslint-disable-next-line */
export interface DashboardHeaderProps {}

export function DashboardHeader(props: DashboardHeaderProps) {
  return (
    <header className={styles.lender_dashboard_header}>

        <a href="javascript:void(0)" className={styles.lender_app_sidebar__toggleBtn}>
          <img src={sidebarOpen} alt="Flowers" />
        </a>
      <div className={styles.lender_dashboard_header__wrapper}>
        <form className={styles.lender_dashboard_header__form}>
          <input
            type="text"
            className={styles.lender_dashboard_header_searchbar}
            placeholder="Search Something...."
          />
          <button
            type="button"
            className={styles.lender_dashboard_header_searchbtn}
          >
            <img src={searchIcon} />
          </button>
        </form>

        <ul>
          <li>
            <a href="/">
              <img src={dashboardSettingIcon} />
            </a>
          </li>

          <li>
            <a href="/">
              <img src={dashboardNotificationIcon} />
            </a>
          </li>
        </ul>
      </div>
    </header>
  );
}

export default DashboardHeader;
