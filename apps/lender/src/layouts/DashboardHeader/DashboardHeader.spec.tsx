import { render } from '@testing-library/react';

import DashboardHeader from './DashboardHeader';

describe('DashboardHeader', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<DashboardHeader />);
    expect(baseElement).toBeTruthy();
  });
});
