import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';

import { Provider } from 'react-redux';
import { RouterProvider } from 'react-router-dom';
import router from './routes';
import store from './app/store';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <Provider store={store}>
    <StrictMode>
      <RouterProvider
        router={router}
        fallbackElement={
          <div>
            <h3>Loading...</h3>
          </div>
        }
      />
    </StrictMode>
  </Provider>
);
