// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { Header } from '@lendinghat/common-ui';

import { Link, Outlet } from 'react-router-dom';

import LogoUrl from '../assets/logo.png';

export function App() {
  return (
    <div>
      {/* <div className="flex justify-center items-center mt-20">
        <Link to={'/login'}>
          <button
            type="button"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          >
            Go to Login
          </button>
        </Link>
        <Link to={'/forgot'}>
          <button
            type="button"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          >
            Forgot Password
          </button>
        </Link>
        <Link to={'/verify'}>
          <button
            type="button"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          >
            Verify OTP
          </button>
        </Link>

        <Link to={'/dashboard'}>
          <button
            type="button"
            className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 mr-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800"
          >
            Dashboard
          </button>
        </Link>
      </div> */}

      <Outlet></Outlet>
    </div>
  );
}

export default App;
