import {
  createAsyncThunk,
  createEntityAdapter,
  createSelector,
  createSlice,
  EntityState,
  PayloadAction,
} from '@reduxjs/toolkit';

export const GET_PROFILE_FEATURE_KEY = 'getProfile';

/*
 * Update these interfaces according to your requirements.
 */
export interface GetProfileEntity {
  token: string;
}

export interface GetProfileState extends EntityState<GetProfileEntity> {
  loadingStatus: 'not loaded' | 'loading' | 'loaded' | 'error';
  error?: string | null;
}

export const getProfileAdapter = createEntityAdapter<GetProfileEntity>();

/**
 * Export an effect using createAsyncThunk from
 * the Redux Toolkit: https://redux-toolkit.js.org/api/createAsyncThunk
 *
 * e.g.
 * ```
 * import React, { useEffect } from 'react';
 * import { useDispatch } from 'react-redux';
 *
 * // ...
 *
 * const dispatch = useDispatch();
 * useEffect(() => {
 *   dispatch(fetchGetProfile())
 * }, [dispatch]);
 * ```
 */
export const fetchGetProfile = createAsyncThunk<GetProfileEntity[]>(
  'getProfile/fetchStatus',
  async (_, thunkAPI) => {
    const response = await fetch(`https://reqres.in/api/login`);
    // Inferred return type: Promise<MyData>
    return await response.json();
  }
);

export const initialGetProfileState: GetProfileState =
  getProfileAdapter.getInitialState({
    loadingStatus: 'not loaded',
    error: null,
  });

export const getProfileSlice = createSlice({
  name: GET_PROFILE_FEATURE_KEY,
  initialState: initialGetProfileState,
  reducers: {
    add: getProfileAdapter.addOne,
    remove: getProfileAdapter.removeOne,
    // ...
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchGetProfile.pending, (state: GetProfileState) => {
        state.loadingStatus = 'loading';
      })
      .addCase(
        fetchGetProfile.fulfilled,
        (state: GetProfileState, action: PayloadAction<GetProfileEntity[]>) => {
          getProfileAdapter.setAll(state, action.payload);
          state.loadingStatus = 'loaded';
        }
      )
      .addCase(fetchGetProfile.rejected, (state: GetProfileState, action) => {
        state.loadingStatus = 'error';
        state.error = action.error.message;
      });
  },
});

/*
 * Export reducer for store configuration.
 */
export const getProfileReducer = getProfileSlice.reducer;

/*
 * Export action creators to be dispatched. For use with the `useDispatch` hook.
 *
 * e.g.
 * ```
 * import React, { useEffect } from 'react';
 * import { useDispatch } from 'react-redux';
 *
 * // ...
 *
 * const dispatch = useDispatch();
 * useEffect(() => {
 *   dispatch(getProfileActions.add({ id: 1 }))
 * }, [dispatch]);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#usedispatch
 */
export const getProfileActions = getProfileSlice.actions;

/*
 * Export selectors to query state. For use with the `useSelector` hook.
 *
 * e.g.
 * ```
 * import { useSelector } from 'react-redux';
 *
 * // ...
 *
 * const entities = useSelector(selectAllGetProfile);
 * ```
 *
 * See: https://react-redux.js.org/next/api/hooks#useselector
 */
const { selectAll, selectEntities } = getProfileAdapter.getSelectors();

export const getGetProfileState = (rootState: {
  [GET_PROFILE_FEATURE_KEY]: GetProfileState;
}): GetProfileState => rootState[GET_PROFILE_FEATURE_KEY];

export const selectAllGetProfile = createSelector(
  getGetProfileState,
  selectAll
);

export const selectGetProfileEntities = createSelector(
  getGetProfileState,
  selectEntities
);
