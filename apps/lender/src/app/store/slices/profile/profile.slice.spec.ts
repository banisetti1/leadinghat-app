import {
  fetchGetProfile,
  getProfileAdapter,
  getProfileReducer,
} from './profile.slice';

describe('profile reducer', () => {
  it('should handle initial state', () => {
    const expected = getProfileAdapter.getInitialState({
      loadingStatus: 'not loaded',
      error: null,
    });

    expect(getProfileReducer(undefined, { type: '' })).toEqual(expected);
  });

  it('should handle fetchProfile', () => {
    let state = getProfileReducer(undefined, fetchGetProfile.pending(''));

    expect(state).toEqual(
      expect.objectContaining({
        loadingStatus: 'loading',
        error: null,
        entities: {},
        ids: [],
      })
    );

    state = getProfileReducer(
      state,
      fetchGetProfile.fulfilled([{ token: 'hfdshd' }], '')
    );

    expect(state).toEqual(
      expect.objectContaining({
        loadingStatus: 'loaded',
        error: null,
        entities: { 1: { id: 1 } },
        ids: [1],
      })
    );

    state = getProfileReducer(
      state,
      fetchGetProfile.rejected(new Error('Uh oh'), '')
    );

    expect(state).toEqual(
      expect.objectContaining({
        loadingStatus: 'error',
        error: 'Uh oh',
        entities: { 1: { id: 1 } },
        ids: [1],
      })
    );
  });
});
