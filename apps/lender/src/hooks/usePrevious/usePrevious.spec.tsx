import { act, renderHook } from '@testing-library/react';
import * as React from 'react';

import usePrevious from './usePrevious';

describe('usePrevious', () => {
  it('should render successfully', () => {
    const { result } = renderHook(() => usePrevious());

    expect(result.current.count).toBe(0);

    act(() => {
      result.current.increment();
    });

    expect(result.current.count).toBe(1);
  });
});
