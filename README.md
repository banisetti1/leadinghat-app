# Lendinghat

<a alt="Lendinghat logo" href="https://lendinghat.com" target="_blank" rel="noreferrer"><img src="https://cnp1845.developer24x7.com/wp-content/uploads/2023/08/Lendinghat-logo.svg" width="90"></a>

## Bootstrap the app

Before start the development run `npm run bootstrap`.

## Start the app

To start the development server with following scripts

### Lender App

```
nx run dev:lender
```

Open your browser and navigate to http://localhost:3001/. Happy coding!

### Merchant App

```
nx run dev:merchant
```

Open your browser and navigate to http://localhost:3002/. Happy coding!

## Running tasks

To execute tasks with Nx use the following syntax:

```
nx <target> <project> <...options>
```

You can also run multiple targets:

```
nx run-many -t <target1> <target2>
```

..or add `-p` to filter specific projects

```
nx run-many -t <target1> <target2> -p <proj1> <proj2>
```

Targets can be defined in the `package.json` or `projects.json`. Learn more [in the docs](https://nx.dev/core-features/run-tasks).

## Ready to deploy?

Just run `nx build demoapp` to build the application. The build artifacts will be stored in the `dist/` directory, ready to be deployed.

## Set up CI!

Nx comes with local caching already built-in (check your `nx.json`). On CI you might want to go a step further.

- [Set up remote caching](https://nx.dev/core-features/share-your-cache)
- [Set up task distribution across multiple machines](https://nx.dev/core-features/distribute-task-execution)
- [Learn more how to setup CI](https://nx.dev/recipes/ci)
