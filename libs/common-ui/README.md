# common-ui

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test common-ui` to execute the unit tests via [Vitest](https://vitest.dev/).

## React Libraries used in common ui

React Toastify [React-Toastify](https://fkhadra.github.io/react-toastify/introduction/).

React Charts [Apex Charts(React)](https://apexcharts.com/react-chart-demos/)

E Charts [E Charts(React)](https://echarts.apache.org/en/index.html)

React Icons [React Icons](https://react-icons.github.io/react-icons/)

Hero Icons [Hero Icons](https://heroicons.com/)
