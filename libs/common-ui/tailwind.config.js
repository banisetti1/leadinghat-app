const { createGlobPatternsForDependencies } = require('@nx/react/tailwind');
const { join } = require('path');

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    join(
      __dirname,
      '{src,pages,components,app, layouts}/**/*!(*.stories|*.spec).{ts,tsx,html}'
    ),
    ...createGlobPatternsForDependencies(__dirname),
  ],
  theme: {
    extend: {
      colors: {
        'lh-border-color': 'rgba(20,107,255,.3)',
        'lh-header-link': '#8B8B8B',
        'lh-blue-dark-gredient':
          'linear-gradient(0deg, #07377E 0%, #07377E 100%), #004176;',

        /* ----blue color presets----*/
        'lh-light-blue-30': '#B9D5FF',
        'lh-light-blue-50': '#48CBFF',
        'lh-light-blue-100': 'rgb(20, 107, 255, 5%)',
        'lh-light-blue-200': 'rgb(20, 107, 255, 10%)',
        'lh-light-blue-250': 'rgb(20, 107, 255, 15%)',
        'lh-blue-light': 'rgb(20, 107, 255, 20%)',
        'lh-blue-light-150': '#DCE9FF',
        'lh-blue-light-100': '#82B0FF',
        'lh-blue-150': 'rgb(20, 107, 255, 30%)',
        'lh-blue-200': 'rgb(20, 107, 255, 40%)',
        'lh-blue-300': 'rgb(20, 107, 255, 60%)',
        'lh-blue-350': 'rgb(20, 107, 255, 70%)',
        'lh-blue-400': '#146BFF',
        'lh-blue-450': '#02A7E9',
        'lh-blue-500': '#477ED0',
        'lh-blue-600': '#02A7E9',
        'lh-blue-dark': '#001C46',

        /* ----dark blue color presets----*/
        'lh-dark-blue-500': '#F1FBFF',
        'lh-dark-blue-800': '#77D8FF',
        'lh-dark-blue-900': '#07377E',

        /* ----black color presets----*/
        'lh-black-20': '#343434',
        'lh-black': '#000',
        'lh-black-50': 'rgb(0, 0, 0, 10%)',
        'lh-black-light-100': '#585858',
        'lh-black-light-300': '#F9F9F9',
        'lh-black-light': '#4E4E4E',
        'lh-black-light-400': '#262626',
        'lh-black-light-500': '#767676',
        'lh-black-100': '#595959',
        'lh-black-350': '#353535',
        'lh-black-450': '#DFDFDF',
        'lh-black-500': '#3C3C3C',
        'lh-black-550': '#5A5A5A',

        /* ----white color presets----*/
        'lh-white': '#FFFFFF',
        'lh-white-50': '#9F9F9F',
        'lh-white-80': '#CACACA',
        'lh-white-90': '#606060',

        /* ----border color presets----*/
        'lh-border-bottom': '#D3D3D3',
        'lh-border-top': '#C8C8C8;',

        /* ----white color presets----*/
        'lh-green-400': '#107300',
        'lh-green-500': '#59DF6F',

        /* ----sky blue color presets----*/
        'lh-skyblue': '#58CFFF',
        'lh-skyblue-10': 'rgba(88, 207, 255, 10%)',
        'lh-skyblue-20': '#96C0FF',
        'lh-skyblue-25': '#4A88E6',
        'lh-skyblue-30': '#2C4975',
        'lh-skyblue-35': 'rgba(183, 213, 255, 20%)',
        'lh-skyblue-40': 'rgba(183, 213, 255, 15%)',
        'lh-skyblue-45': '#4642F9',
        'lh-skyblue-50': 'rgba(183, 213, 255, 60%)',
        'lh-skyblue-55': 'rgba(185, 213, 255, 40%)',

        /* ----green color presets----*/
        'lh-success': '#169B00',
        'lh-success-10': 'rgba(22, 155, 0, 10%)',
        'lh-success-20': '#21C206',
        'lh-success-30': '#25D882',

        /* ----red color presets----*/
        'lh-danger': '#C10606',
        'lh-danger-10': 'rgba(193, 6, 6, 10%)',
        'lh-danger-20': '#DB2808',

        /* ----other color presets----*/
        'lh-requred': '#D40000',
        'lh-magenta-10': '#D41387',
        'lh-yellow-10': '#F9C91F',
        'lh-blue-10': '#4642F9',
        'lh-orange-10': '#F06E25',
      },
      fontFamily: {
        poppins: ['Poppins', 'sans-serif'],
      },
      container: {
        maxWidth: {
          DEFAULT: '100%',
          sm: '640px',
          md: '768px',
          lg: '992px',
          xl: '1200px',
          '2xl': '1300px',
          '3xl': '1300px',
          '4xl': '1300px',
        },
      },
      spacing: {
        '9/16': '56.25%',
      },
      fontWeight: {
        thin: '100',
        hairline: '100',
        extralight: '200',
        light: '300',
        normal: '400',
        medium: '500',
        semibold: '600',
        bold: '700',
        extrabold: '800',
        black: '900',
      },
    },
  },
  plugins: [],
};
