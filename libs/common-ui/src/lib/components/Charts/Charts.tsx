import ReactApexChart from 'react-apexcharts';
import styles from './Charts.module.scss';
import Line from './Line';
import Stacked from './Stacked';

/* eslint-disable-next-line */
export interface ChartsProps {}

export function Charts(props: ChartsProps) {
  return (
    <>
      <Line />;
      <Stacked />
    </>
  );
}

export default Charts;
