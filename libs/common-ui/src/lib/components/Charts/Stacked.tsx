import React, { Component } from 'react';
import ReactApexChart from 'react-apexcharts';

class Stacked extends Component {
  constructor(props: any) {
    super(props);

    this.state = {
      options: {
        responsive: [
          {
            breakpoint: 480,
            options: {
              legend: {
                position: 'bottom',
                offsetX: -10,
                offsetY: 0,
              },
            },
          },
        ],
        chart: {
          type: 'bar',
          height: 350,
          stacked: true,
          toolbar: {
            show: true,
          },
          zoom: {
            enabled: true,
          },
        },
        plotOptions: {
          bar: {
            horizontal: false,
            borderRadius: 10,
            dataLabels: {
              total: {
                enabled: true,
                style: {
                  fontSize: '13px',
                  fontWeight: 900,
                },
              },
            },
          },
        },

        xaxis: {
          type: 'datetime',
          categories: ['1W', '2W', '3W', '4W'],
        },
        legend: {
          position: 'right',
          offsetY: 40,
        },
        fill: {
          opacity: 1,
        },
      },
      series: [
        {
          name: 'Medical Practices',
          data: [44, 55, 41, 67],
        },
        {
          name: 'Commercial Vehicles',
          data: [13, 23, 20, 8],
        },
        {
          name: 'Construction',
          data: [11, 17, 15, 15],
        },
        {
          name: 'Restaurants',
          data: [21, 7, 25, 13],
        },
      ],
    };
  }

  render() {
    return (
      <div className="line">
        <ReactApexChart
          options={(this.state as any).options}
          series={(this.state as any).series}
          type="bar"
          width="500"
        />
      </div>
    );
  }
}

export default Stacked;
