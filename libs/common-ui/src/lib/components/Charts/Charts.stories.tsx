import type { Meta, StoryObj } from '@storybook/react';
import { Charts, ChartsProps } from './Charts';

const meta: Meta<typeof Charts> = {
  component: Charts,
  title: 'Common-UI/Components/Charts',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=225-5256&mode=design&t=u3c7tyJu34py3ZeP-4',
    },
  },
};
export default meta;

export const Primary = {
  render: (args: ChartsProps) => {
    return <Charts {...args} />;
  },
};
