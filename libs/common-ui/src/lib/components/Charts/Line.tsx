import React, { Component } from 'react';
import ReactApexChart from 'react-apexcharts';

class Line extends Component {
  constructor(props: any) {
    super(props);

    this.state = {
      options: {
        stroke: {
          curve: 'smooth',
        },
        chart: {
          toolbar: {
            show: false,
          },
        },
        colors: ['#D41387', '#F9C91F', '#4642F9'],
        markers: {
          size: 0,
        },
        xaxis: {
          categories: ['Apr', 'May', 'Jun'],
        },
        yaxis: {
          lines: {
            show: true,
          },
          labels: {
            formatter: function (value: any) {
              return value === 0 ? value : value + 'k';
            },
          },
          tickAmount: 5,
          min: 0,
          max: 100,
        },
        grid: {
          show: true,
          borderColor: '#D9E7FF',
          strokeDashArray: 3,
        },
      },
      series: [
        {
          name: 'Completed',
          data: [58, 85, 50],
        },
        {
          name: 'Active',
          data: [45, 55, 20],
        },
        {
          name: 'Default',
          data: [30, 20, 10],
        },
      ],
    };
  }

  render() {
    return (
      <div className="line">
        <ReactApexChart
          options={(this.state as any).options as any}
          series={(this.state as any).series as any}
          type="line"
          width="500"
        />
      </div>
    );
  }
}

export default Line;
