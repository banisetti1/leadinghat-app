import { render } from '@testing-library/react';

import Charts from './Charts';

describe('Charts', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Charts />);
    expect(baseElement).toBeTruthy();
  });
});
