import type { Meta, StoryObj } from '@storybook/react';
import { TextInput, TextInputProps } from './TextInput';

const meta: Meta<typeof TextInput> = {
  component: TextInput,
  title: 'Common-UI/Components/TextInput',
};
export default meta;

export const Primary = {
  render: (args: TextInputProps) => {
    return <TextInput {...args} />;
  },
  args: {
    placeholder: 'Enter First Name',
  },
};
