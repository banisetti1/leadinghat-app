import type { Meta, StoryObj } from '@storybook/react';
import { Avatar, AvatarProps } from './Avatar';

const meta: Meta<typeof Avatar> = {
  component: Avatar,
  title: 'Common-UI/Components/Avatar',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=176-274&mode=design&t=OIf1sakvDdLiyAXk-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: AvatarProps) => {
    return <Avatar {...args} />;
  },
};
