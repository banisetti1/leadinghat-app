import styles from './Avatar.module.scss';

/* eslint-disable-next-line */
export interface AvatarProps {}

export function Avatar(props: AvatarProps) {
  return (
    <div className="flex">
      <div className="relative">
        <img
          className="w-10 h-10 rounded-full"
          src="https://i.pravatar.cc/300"
          alt=""
        />
        <span className="top-0 left-7 absolute  w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full"></span>
      </div>
      <div className="relative">
        <img
          className="w-10 h-10 rounded"
          src="https://i.pravatar.cc/300"
          alt=""
        />
        <span className="absolute top-0 left-8 transform -translate-y-1/2 w-3.5 h-3.5 bg-red-400 border-2 border-white dark:border-gray-800 rounded-full"></span>
      </div>
      <div className="relative">
        <img
          className="w-10 h-10 rounded-full"
          src="https://i.pravatar.cc/300"
          alt=""
        />
        <span className="bottom-0 left-7 absolute  w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full"></span>
      </div>
      <div className="relative">
        <img
          className="w-10 h-10 rounded"
          src="https://i.pravatar.cc/300"
          alt=""
        />
        <span className="absolute bottom-0 left-8 transform translate-y-1/4 w-3.5 h-3.5 bg-green-400 border-2 border-white dark:border-gray-800 rounded-full"></span>
      </div>
    </div>
  );
}

export default Avatar;
