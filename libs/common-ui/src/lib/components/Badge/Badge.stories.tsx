import type { Meta, StoryObj } from '@storybook/react';
import { Badge, BadgeProps } from './Badge';

const meta: Meta<typeof Badge> = {
  component: Badge,
  title: 'Common-UI/Components/Badge',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=176-274&mode=design&t=OIf1sakvDdLiyAXk-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: BadgeProps) => {
    return <Badge {...args} />;
  },
};
