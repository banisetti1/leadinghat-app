import styles from './Card.module.scss';

/* eslint-disable-next-line */
export interface CardProps {
  bgColor?: string;
  textColor?: string;
  subHeading: string;
  amount: string;
  footer?: string;
}

export function Card(props: CardProps) {
  const { bgColor, subHeading, amount, footer, textColor } = props;
  return (
    <a
      href="/"
      className={`block w-screen p-6 ${
        bgColor ?? 'bg-white'
      } border border-gray-200 rounded-lg shadow ${
        bgColor ? '' : 'hover:bg-gray-100'
      } dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700`}
    >
      <p
        className={`font-normal mb-4 ${
          textColor ?? 'text-gray-700'
        }  dark:text-gray-400`}
      >
        {subHeading}
      </p>
      <h5
        className={`mb-2 text-2xl font-bold tracking-tight ${
          textColor ?? 'text-gray-900'
        } dark:text-white`}
      >
        {amount}
      </h5>
      <p className="mb-3 mt-3 font-normal text-xs text-gray-700 dark:text-gray-400">
        {footer}
      </p>
      {/* <h5 className="mb-2 text-xs font-normal tracking-tight text-gray-900 dark:text-white">
        20-07-2023
      </h5> */}
    </a>
  );
}

export default Card;
