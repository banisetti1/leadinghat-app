import type { Meta, StoryObj } from '@storybook/react';
import { Card, CardProps } from './Card';

const meta: Meta<typeof Card> = {
  component: Card,
  title: 'Common-UI/Components/Card',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=225-5256&mode=design&t=u3c7tyJu34py3ZeP-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: CardProps) => {
    return <Card {...args} />;
  },
};
