import styles from './Text.module.scss';

type TextAs =
  | 'h1'
  | 'h2'
  | 'h3'
  | 'h4'
  | 'h5'
  | 'h6'
  | 'p'
  | 'p_black'
  | 'p_light'
    'p_11';

/* eslint-disable-next-line */
export interface TextProps {
  as: TextAs;
  children: React.ReactNode;
  color?: string;
  size?: number;
}

export function Text(props: TextProps) {
  const { as, children } = props;

  switch (as) {
    case 'h1':
      return <h1 className={styles[as]}>{children}</h1>;

    case 'h2':
      return <h2 className={styles[as]}>{children}</h2>;

    case 'h3':
      return <h3 className={styles[as]}>{children}</h3>;

    case 'h4':
      return <h4 className={styles[as]}>{children}</h4>;

    case 'h5':
      return <h5 className={styles[as]}>{children}</h5>;

    case 'h6':
      return <h6 className={styles[as]}>{children}</h6>;

    case 'p':
      return <p className={styles[as]}>{children}</p>;

    case 'p_black':
      return <p className={styles[as]}>{children}</p>;

    case 'p_light':
      return <p className={styles[as]}>{children}</p>;

      case 'p_11':
      return <p className={styles[as]}>{children}</p>;

    default:
      throw new Error('Please specify text as prop');
  }
}

export default Text;
