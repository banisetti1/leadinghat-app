import type { Meta, StoryObj } from '@storybook/react';
import { CheckBox, CheckBoxProps } from './CheckBox';

const meta: Meta<typeof CheckBox> = {
  component: CheckBox,
  title: 'Common-UI/Components/CheckBox',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=103-4701&mode=design&t=OIf1sakvDdLiyAXk-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: CheckBoxProps) => {
    return <CheckBox {...args} />;
  },
};
