import styles from './Container.module.scss';

/* eslint-disable-next-line */
export interface ContainerProps {
  children: React.ReactNode;
  style?: {
    [key: string]: string;
  };
}

export function Container({ children, style }: ContainerProps) {
  return (
    <div
      className={`${styles['container']} ${style ?? ''}`}
      // style={style}
    >
      {children}
    </div>
  );
}

export default Container;
