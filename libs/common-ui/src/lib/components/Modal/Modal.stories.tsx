import type { Meta, StoryObj } from '@storybook/react';
import { Modal, ModalProps } from './Modal';

const meta: Meta<typeof Modal> = {
  component: Modal,
  title: 'Common-UI/Components/Modal',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=176-4753&mode=design&t=OIf1sakvDdLiyAXk-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: ModalProps) => {
    return <Modal {...args} />;
  },
};
