import styles from './Modal.module.scss';

/* eslint-disable-next-line */
export interface ModalProps {}

export function Modal(props: ModalProps) {
  return (
    <div
      id="popup-modal"
      tabIndex={-1}
      className="fixed top-0 left-0 right-0 z-50 p-4 block overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] max-h-full"
    >
      <div className="relative w-full max-w-md max-h-full">
        <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
          <button
            type="button"
            className="absolute top-3 right-2.5 text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ml-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white"
          >
            <svg
              className="w-3 h-3"
              aria-hidden="true"
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 14 14"
            >
              <path
                stroke="currentColor"
                stroke-linecap="round"
                stroke-linejoin="round"
                stroke-width="2"
                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"
              />
            </svg>
            <span className="sr-only">Close modal</span>
          </button>
          <div className="p-6 text-center">
            <svg
              className="mx-auto mb-4 text-gray-400 w-12 h-12 dark:text-gray-200"
              width="68"
              height="68"
              viewBox="0 0 68 68"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M33.9997 0.0834961C27.2916 0.0834961 20.7342 2.07267 15.1566 5.79948C9.57903 9.5263 5.23185 14.8234 2.66478 21.0208C0.0977035 27.2183 -0.57396 34.0378 0.734722 40.617C2.0434 47.1962 5.27365 53.2395 10.017 57.9829C14.7603 62.7262 20.8037 65.9564 27.3829 67.2651C33.9621 68.5738 40.7816 67.9021 46.979 65.3351C53.1765 62.768 58.4736 58.4208 62.2004 52.8433C65.9272 47.2657 67.9164 40.7082 67.9164 34.0002C67.9164 25.0049 64.343 16.3781 57.9824 10.0175C51.6218 3.65685 42.9949 0.0834961 33.9997 0.0834961ZM51.5963 26.9301L30.0129 48.5134C29.4347 49.0914 28.6506 49.4162 27.833 49.4162C27.0154 49.4162 26.2313 49.0914 25.6531 48.5134L16.4031 39.2634C15.8415 38.6819 15.5307 37.903 15.5377 37.0946C15.5447 36.2862 15.869 35.5128 16.4407 34.9411C17.0123 34.3695 17.7857 34.0452 18.5941 34.0382C19.4026 34.0311 20.1814 34.3419 20.7629 34.9036L27.833 41.9737L47.2364 22.5702C47.818 22.0086 48.5968 21.6978 49.4053 21.7048C50.2137 21.7119 50.987 22.0361 51.5587 22.6078C52.1304 23.1795 52.4547 23.9528 52.4617 24.7613C52.4687 25.5697 52.1579 26.3486 51.5963 26.9301Z"
                fill="#146BFF"
              />
            </svg>

            <h3 className="mb-5 text-2xl font-bold text-gray-500 dark:text-gray-400">
              Thank You!
            </h3>
            <p>
              Thanks for choosing this offer from Gynger. sales agent will
              contact you shortly.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Modal;
