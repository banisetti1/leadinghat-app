import * as React from 'react';
import styles from './Table.module.scss';

import {
  ColumnDef,
  createColumnHelper,
  flexRender,
  getCoreRowModel,
  getFilteredRowModel,
  useReactTable,
} from '@tanstack/react-table';
import Button from '../Button/Button';
import { DateIconDark } from '@lendinghat/icons';

import { RDatePicker as DatePicker } from '../DatePicker/DatePicker';

type Application = {
  applicationId: string;
  loanType: string;
  loanSource: string;
  amount: string;
  merchant: string;
  industry: string;
  revenue: number;
  date: number;
  status: boolean;
};

const defaultData: Application[] = [
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: true,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: true,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: false,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: true,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: true,
  },
  {
    applicationId: 'BLC39856',
    loanType: 'Working Capital',
    loanSource: 'Lending Hat',
    amount: '$50,000',
    merchant: 'Data MacLead',
    industry: 'Construction',
    revenue: 15000,
    date: 15,
    status: true,
  },
  
];
/* eslint-disable-next-line */
export interface TableProps {}

export function Table(props: TableProps) {
  const [data, setData] = React.useState(() => [...defaultData]);

  const [rowSelection, setRowSelection] = React.useState({});

  const columns = React.useMemo<ColumnDef<Application>[]>(
    () => [
      {
        id: 'select',
        header: ({ table }) => (
          <div>
          <label htmlFor='default-checkbox' className={styles.table_checkbox_label}>
            <input
              id="default-checkbox"
              type="checkbox"
              value=""
              className=""
              {...{
                checked: table.getIsAllRowsSelected(),
                indeterminate: table.getIsSomeRowsSelected(),
                onChange: table.getToggleAllRowsSelectedHandler(),
              }}
            />
            <span className={styles.table_checkbox}></span>
          </label>
          </div>
        ),
        cell: ({ row }) => (
          <div className="px-1">
            
            <label htmlFor='default-checkbox' className={styles.table_checkbox_label}>
            <input
              id="default-checkbox"
              type="checkbox"
              value=""
              className=""
              {...{
                checked: table.getIsAllRowsSelected(),
                indeterminate: table.getIsSomeRowsSelected(),
                onChange: table.getToggleAllRowsSelectedHandler(),
              }}
            />
            <span className={styles.table_checkbox}></span>
          </label>
          </div>
        ),
      },
      {
        header: 'Application Id',
        accessorKey: 'applicationId',
        cell: (info) => info.getValue(),
      },
      {
        header: 'Loan Type',
        accessorKey: 'loanType',
        cell: (info) => info.getValue(),
      },
      {
        header: 'Loan Source',
        accessorKey: 'loanSource',
        cell: (info) => info.getValue(),
      },
      {
        header: 'Amount',
        accessorKey: 'amount',
        cell: (info) => info.getValue(),
      },
      {
        header: 'Merchant',
        accessorKey: 'merchant',
        cell: (info) => info.getValue(),
      },
      {
        header: 'Industry',
        accessorKey: 'industry',
        cell: (info) => info.getValue(),
      },
      {
        header: 'Revenue',
        accessorKey: 'revenue',
        cell: (info) => (
          <span>${info.getValue() as React.ReactNode | string}</span>
        ),
      },
      {
        header: 'Date',
        accessorKey: 'date',
        cell: (info) => {
          return (
            <DatePicker
              onChange={function (date: Date | null): void {
                throw new Error('Function not implemented.');
              }}
              selected={null}
            />
          );
        },
      },
      {
        header: 'Status',
        accessorKey: 'status',
        cell: (info) => {
          return (
            <div className="flex row row-span-2 items-center gap-x-[10px] mr-0">
              <Button label={'Need Review'} variant={'outline-info'} 
              className={styles.table_info_btn}/>

              <Button
                label={info.getValue() ? 'Approved' : 'Declined'}
                variant={info.getValue() ? 'outline-success' : 'outline-danger'}
                className={styles.table_success_btn}
              />
              
              <Button label={'Underwrite'} variant={'primary'} 
              className={styles.table_primary_btn}/>

              <Button label={' '} variant={'trash-btn'} 
              className={styles.table_primary_btn}/>
            </div>
          );
        },
      },
      
    ],
    []
  );

  const table = useReactTable({
    data,
    columns,
    state: {
      rowSelection,
    },
    enableRowSelection: true, //enable row selection for all rows
    // enableRowSelection: row => row.original.age > 18, // or enable row selection conditionally per row
    onRowSelectionChange: setRowSelection,
    getCoreRowModel: getCoreRowModel(),
    getFilteredRowModel: getFilteredRowModel(),
    debugTable: true,
  });

  return (
    <div className={styles.dashboard_table_wrapper}>
      <table className={styles.table}>
        <thead>
          {table.getHeaderGroups().map((headerGroup) => (
            <tr key={headerGroup.id}>
              {headerGroup.headers.map((header) => (
                <th key={header.id}>
                  {header.isPlaceholder
                    ? null
                    : flexRender(
                        header.column.columnDef.header,
                        header.getContext()
                      )}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody>
          {table.getRowModel().rows.map((row) => (
            <tr key={row.id} className={styles.table_row}>
              {row.getVisibleCells().map((cell) => (
                <td key={cell.id}>
                  {flexRender(cell.column.columnDef.cell, cell.getContext())}
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default Table;
