import { flexRender } from '@tanstack/react-table';
import React from 'react';

interface ITableHeader {
  table: any;
  type: any;
}

const TableHeader = (props: ITableHeader) => {
  const { table } = props;
  return (
    <thead>
      {table.getHeaderGroups().map((headerGroup: any) => (
        <tr
          key={headerGroup.id}
          className="p-6 bg-white border border-gray-200 rounded-lg shadow"
        >
          {headerGroup.headers.map((header: any) => (
            <th key={header.id}>
              {header.isPlaceholder
                ? null
                : flexRender(
                    header.column.columnDef.header,
                    header.getContext()
                  )}
            </th>
          ))}
        </tr>
      ))}
    </thead>
  );
};

export default TableHeader;
