import type { Meta, StoryObj } from '@storybook/react';
import { Table, TableProps } from './Table';

const meta: Meta<typeof Table> = {
  component: Table,
  title: 'Common-UI/Components/Table',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=225-5435&mode=design&t=u3c7tyJu34py3ZeP-4',
    },
  },
};
export default meta;

export const Primary = {
  render: (args: TableProps) => {
    return <Table {...args} />;
  },
};
