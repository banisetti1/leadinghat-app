import cx from 'clsx';
import styles from './Button.module.scss';
/* eslint-disable-next-line */

export type ButtonVariant =
  | 'default'
  | 'success'
  | 'danger'
  | 'error'
  | 'warning'
  | 'link'
  | 'primary'
  | 'outline'
  | 'rounded'
  | 'outline-light'
  | 'outline-dark'
  | 'outline-info'
  | 'outline-success'
  | 'outline-danger'
  | 'trash-btn';

export interface ButtonProps {
  icon?: string;
  rightIcon?: string;
  rightIconStyle?: React.CSSProperties;
  leftIcon?: string;
  leftIconStyle?: React.CSSProperties;
  iconStyle?: React.CSSProperties;
  iconVariant?: 'default' | 'dark' | 'light';
  label?: React.ReactNode;
  title?: string;
  variant?: ButtonVariant;
  isOptionButton?: boolean;
  isDark?: boolean;
  className?: string;
  forwardedRef?: React.RefObject<HTMLButtonElement>;
  onClick?: (e: React.MouseEvent<HTMLButtonElement>) => void;
  [key: string]: unknown;
}

export const Button: React.FC<ButtonProps> = (props) => {
  const {
    // eslint-disable-line react/prop-types
    type,
    className,
    forwardedRef,
    label,
    variant,
    ...restProps
  } = props;

  if (!label) {
    throw new Error(
      'You must provide at least a button label!\n' +
        "'label' may be any valid React node."
    );
  }

  const classes = cx(
    styles.primary,
    {
      [styles.outlineLight]: variant === 'outline-light',
      [styles.outlineDark]: variant === 'outline-dark',
      [styles.outlineInfo]: variant === 'outline-info',
      [styles.outlineSuccess]: variant === 'outline-success',
      [styles.outlineDanger]: variant === 'outline-danger',
      [styles.Trash]: variant === 'trash-btn',
      [styles.TextLink]: variant === 'link',
    },
    className
  );

  return (
    <button {...restProps} className={classes}>
      {label}
      <svg
        width="11"
        height="9"
        viewBox="0 0 11 9"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M10.3536 4.85355C10.5488 4.65829 10.5488 4.34171 10.3536 4.14645L7.17157 0.964467C6.97631 0.769205 6.65973 0.769204 6.46447 0.964467C6.2692 1.15973 6.2692 1.47631 6.46447 1.67157L9.29289 4.5L6.46447 7.32843C6.2692 7.52369 6.2692 7.84027 6.46447 8.03553C6.65973 8.2308 6.97631 8.2308 7.17157 8.03553L10.3536 4.85355ZM-4.37114e-08 5L10 5L10 4L4.37114e-08 4L-4.37114e-08 5Z"
          fill=""
        />
      </svg>
    </button>
  );
};

export default Button;
