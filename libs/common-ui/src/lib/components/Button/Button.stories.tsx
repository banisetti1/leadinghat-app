import type { Meta, StoryObj } from '@storybook/react';
import { Button, ButtonProps } from './Button';

const meta: Meta<typeof Button> = {
  component: Button,
  title: 'Common-UI/Components/Button',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=225-5435&mode=design&t=u3c7tyJu34py3ZeP-4',
    },
  },
  argTypes: {
    variant: {
      options: [
        'primary',
        'outline-light',
        'outline-info',
        'outline-success',
        'outline-danger',
      ],
      control: { type: 'select' },
    },
    onClick: {
      action: 'clicked',
    },
  },
};
export default meta;

export const Primary = {
  render: (args: ButtonProps) => {
    return <Button {...args} />;
  },
  args: {
    label: 'Button',
    variant: 'primary',
  },
};

export const OutlineLight = {
  render: (args: ButtonProps) => {
    return <Button {...args} />;
  },
  args: {
    label: 'Ouline Light Button',
    variant: 'outline-light',
  },
};

export const OutlineInfo = {
  render: (args: ButtonProps) => {
    return <Button {...args} />;
  },
  args: {
    label: 'Outline Info Button',
    variant: 'outline-info',
  },
};
export const OutlineSuccess = {
  render: (args: ButtonProps) => {
    return <Button {...args} />;
  },
  args: {
    label: 'Outline Success Button',
    variant: 'outline-success',
  },
};
export const OutlineDanger = {
  render: (args: ButtonProps) => {
    return <Button {...args} />;
  },
  args: {
    label: 'Outline Danger Button',
    variant: 'outline-danger',
  },
};
