import { render } from '@testing-library/react';

import DatePicker from './DatePicker';

describe('DatePicker', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <DatePicker
        onChange={function (date: Date | null): void {
          throw new Error('Function not implemented.');
        }}
        selected={null}
      />
    );
    expect(baseElement).toBeTruthy();
  });
});
