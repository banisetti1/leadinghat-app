import type { Meta, StoryObj } from '@storybook/react';
import { RDatePicker, DatePickerProps } from './DatePicker';

const meta: Meta<typeof RDatePicker> = {
  component: RDatePicker,
  title: 'Common-UI/Components/DatePicker',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=225-5435&mode=design&t=u3c7tyJu34py3ZeP-4',
    },
  },
  argTypes: {},
};
export default meta;

export const Primary = {
  render: (args: DatePickerProps) => {
    return <RDatePicker {...args} />;
  },
};
