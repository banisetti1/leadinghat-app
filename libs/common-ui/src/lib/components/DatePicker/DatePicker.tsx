import { forwardRef } from 'react';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import cx from 'clsx';
import { DateIconDark, DateIconLight } from '@lendinghat/icons';
import styles from './DatePicker.module.scss';

/* eslint-disable-next-line */
export interface DatePickerProps {
  className?: string;
  onChange: (date: Date | null) => void;
  selected: Date | null;
  placeholder?: string;
  dark?: boolean;
  placeholderStyle?: string;
}

export function RDatePicker(props: DatePickerProps) {
  const { className, onChange, selected, placeholder, dark, placeholderStyle } =
    props;

  const placeholderContainerClasses = cx(styles.placeholder_light, {
    [styles.placeholder_dark]: dark === true,
  });

  return (
    <div className={className ?? ''}>
      <div className={placeholderContainerClasses}>
        <p className={placeholderStyle}>{placeholder}</p>
      </div>
      <DatePicker
        closeOnScroll={true}
        selected={selected}
        onChange={onChange}
        customInput={dark ? <DateIconLight /> : <DateIcon />}
        popperPlacement="bottom"
        popperModifiers={[
          {
            name: 'offset',
            options: {
              offset: [5, 10],
            },
          },
          {
            name: 'preventOverflow',
            options: {
              rootBoundary: 'viewport',
              tether: false,
              altAxis: true,
            },
          },
        ]}
      />
    </div>
  );
}

export default RDatePicker;

export const DateIcon = forwardRef<any>(({ onClick }, ref) => (
  <div onClick={onClick}>
    <DateIconDark />
  </div>
));
