import type { Meta, StoryObj } from '@storybook/react';
import { RadioButton, RadioButtonProps } from './RadioButton';

const meta: Meta<typeof RadioButton> = {
  component: RadioButton,
  title: 'Common-UI/Components/RadioButton',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=103-4701&mode=design&t=OIf1sakvDdLiyAXk-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: RadioButtonProps) => {
    return <RadioButton {...args} />;
  },
};
