import type { Meta, StoryObj } from '@storybook/react';
import { Dropdown, DropdownProps } from './Dropdown';

const meta: Meta<typeof Dropdown> = {
  component: Dropdown,
  title: 'Common-UI/Components/Dropdown',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=225-5512&mode=design&t=u3c7tyJu34py3ZeP-4',
    },
  },
};
export default meta;

export const Primary = {
  render: (args: DropdownProps) => {
    return <Dropdown {...args} />;
  },
};
