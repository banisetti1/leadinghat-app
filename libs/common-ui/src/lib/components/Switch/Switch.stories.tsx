import type { Meta, StoryObj } from '@storybook/react';
import { Switch, SwitchProps } from './Switch';

const meta: Meta<typeof Switch> = {
  component: Switch,
  title: 'Common-UI/Components/Switch',
  parameters: {
    design: {
      type: 'figma',
      url: 'https://www.figma.com/file/Rn1Cp2Os0VDyvkfiSubteg/Final-design-of-Application-Screens?type=design&node-id=176-4102&mode=design&t=OIf1sakvDdLiyAXk-4',
    },
  },
};
export default meta;

export const Basic = {
  render: (args: SwitchProps) => {
    return <Switch {...args} />;
  },
};
