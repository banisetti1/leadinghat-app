import { Link } from 'react-router-dom';
import styles from './Header.module.scss';

/* eslint-disable-next-line */
export interface HeaderProps {
  logo: string;
}

export function Header(props: HeaderProps) {
  const { logo } = props;
  return (
    <header
      className={`${styles.lh_c_header} shadow-blue-100/50 shadow border-b-2 borer-blue-400`}
    >
      <div className={`${styles.container} container mx-auto`}>
        <div className={`row ${styles.lh_c_header__row}`}>
          <div className={styles.lh_c_logo}>
            <Link to="/">
              <span className="sr-only">Lending Hat</span>
              <img
                src={logo}
                alt="lending hat logo"
                height={55.45}
                width={202}
              />
            </Link>
          </div>

          <div className={styles.lh_c_cta}>
            <a href={'/'} className={`${styles.lh_u_btn}`}>
              Already have an account?
            </a>
            <Link to={'/login'} className="text-blue-600 font-bold">
              Log in
            </Link>
            {/* <a
              href={'/'}
              className={`${styles.lh_u_btn} ${styles.lh_u_btn__solid} ${styles.lh_u_btn__solid__icon} ${styles.lh_u_btn__rounded}`}
            >
              Schedule a Demo
            </a> */}
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
