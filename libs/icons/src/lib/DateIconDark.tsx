import * as React from 'react';
import type { SVGProps } from 'react';
import { memo } from 'react';
const SvgDateIconDark = (props: SVGProps<SVGSVGElement>) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={24}
    height={24}
    fill="none"
    {...props}
  >
    <path
      fill="#146BFF"
      fillRule="evenodd"
      d="M.75 8.25a5 5 0 0 1 5-5h12.5a5 5 0 0 1 5 5v10a5 5 0 0 1-5 5H5.75a5 5 0 0 1-5-5v-10Zm5-2.5a2.5 2.5 0 0 0-2.5 2.5v10a2.5 2.5 0 0 0 2.5 2.5h12.5a2.5 2.5 0 0 0 2.5-2.5v-10a2.5 2.5 0 0 0-2.5-2.5H5.75Z"
      clipRule="evenodd"
    />
    <path
      fill="#146BFF"
      fillRule="evenodd"
      d="M7 .75c.69 0 1.25.56 1.25 1.25v2.5a1.25 1.25 0 0 1-2.5 0V2c0-.69.56-1.25 1.25-1.25ZM17 .75c.69 0 1.25.56 1.25 1.25v2.5a1.25 1.25 0 1 1-2.5 0V2c0-.69.56-1.25 1.25-1.25Z"
      clipRule="evenodd"
    />
    <path
      fill="#146BFF"
      d="M8.25 9.5a1.25 1.25 0 1 1-2.5 0 1.25 1.25 0 0 1 2.5 0ZM13.25 9.5a1.25 1.25 0 1 1-2.5 0 1.25 1.25 0 0 1 2.5 0ZM18.25 9.5a1.25 1.25 0 1 1-2.5 0 1.25 1.25 0 0 1 2.5 0ZM8.25 14.5a1.25 1.25 0 1 1-2.5 0 1.25 1.25 0 0 1 2.5 0Z"
    />
  </svg>
);
const Memo = memo(SvgDateIconDark);
export default Memo;
