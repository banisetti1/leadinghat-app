const path = require('path');

function defaultIndexTemplate(filePaths) {
  const exportEntries = filePaths.map(({ path: filePath }) => {
    const basename = path.basename(filePath, path.extname(filePath));
    console.log('DefaultPath', basename, path.join(__dirname, '../'));
    const exportName = /^\d/.test(basename) ? `Svg${basename}` : basename;
    return `export { default as ${exportName} } from './lib/${basename}'`;
  });
  return exportEntries.join('\n');
}

module.exports = defaultIndexTemplate;
