import * as React from 'react';
import type { SVGProps } from 'react';
import { memo } from 'react';
const SvgSecureIcon = (props: SVGProps<SVGSVGElement>) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={26}
    height={26}
    fill="none"
    {...props}
  >
    <path
      stroke="#fff"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeMiterlimit={10}
      strokeWidth={2}
      d="M20.313 5.444c-2.763 0-5.363-1.138-7.313-3.006-1.95 1.868-4.55 3.006-7.313 3.006-1.137 0-2.193-.244-3.25-.569C2.438 16.25 6.907 20.8 13 23.563 19.094 20.8 23.563 16.25 23.563 4.875c-1.057.325-2.113.569-3.25.569Z"
    />
    <path
      stroke="#fff"
      strokeLinecap="round"
      strokeLinejoin="round"
      strokeMiterlimit={10}
      strokeWidth={2}
      d="m9.75 13 2.438 2.438 4.874-4.876"
    />
  </svg>
);
const Memo = memo(SvgSecureIcon);
export default Memo;
