import * as React from 'react';
import type { SVGProps } from 'react';
import { memo } from 'react';
const SvgDateIconLight = (props: SVGProps<SVGSVGElement>) => (
  <svg
    xmlns="http://www.w3.org/2000/svg"
    width={14}
    height={14}
    fill="none"
    {...props}
  >
    <path
      fill="#fff"
      fillRule="evenodd"
      d="M.25 4.75a3 3 0 0 1 3-3h7.5a3 3 0 0 1 3 3v6a3 3 0 0 1-3 3h-7.5a3 3 0 0 1-3-3v-6Zm3-1.5a1.5 1.5 0 0 0-1.5 1.5v6a1.5 1.5 0 0 0 1.5 1.5h7.5a1.5 1.5 0 0 0 1.5-1.5v-6a1.5 1.5 0 0 0-1.5-1.5h-7.5Z"
      clipRule="evenodd"
    />
    <path
      fill="#fff"
      fillRule="evenodd"
      d="M4 .25a.75.75 0 0 1 .75.75v1.5a.75.75 0 0 1-1.5 0V1A.75.75 0 0 1 4 .25ZM10 .25a.75.75 0 0 1 .75.75v1.5a.75.75 0 0 1-1.5 0V1A.75.75 0 0 1 10 .25Z"
      clipRule="evenodd"
    />
    <path
      fill="#fff"
      d="M4.75 5.5a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM7.75 5.5a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM10.75 5.5a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0ZM4.75 8.5a.75.75 0 1 1-1.5 0 .75.75 0 0 1 1.5 0Z"
    />
  </svg>
);
const Memo = memo(SvgDateIconLight);
export default Memo;
